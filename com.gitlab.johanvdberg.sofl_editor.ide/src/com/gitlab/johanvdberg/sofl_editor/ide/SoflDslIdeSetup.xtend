/*
 * generated by Xtext 2.14.0
 */
package com.gitlab.johanvdberg.sofl_editor.ide

import com.gitlab.johanvdberg.sofl_editor.SoflDslRuntimeModule
import com.gitlab.johanvdberg.sofl_editor.SoflDslStandaloneSetup
import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class SoflDslIdeSetup extends SoflDslStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new SoflDslRuntimeModule, new SoflDslIdeModule))
	}
	
}
