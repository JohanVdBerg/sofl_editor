package com.gitlab.johanvdberg.sofl_editor.generator.smt

import com.gitlab.johanvdberg.sofl_editor.smt.SmtConditionNode
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFile

class SmtProveControl {
	public SmtConditionNode node
	SmtFile smt_file
	
	new(SmtConditionNode node, String file_prefix){
		this.node = node
		this.smt_file = new SmtFile(
			file_prefix, '''module_invariant_«this.node.name».smt''', 
			SmtFile.ProofType::Constrain, 
			this.node.transition_obligations.values.head.smt_proof
		)
	}
	
	def write_markdown(){
		'''
		Condition: «node.get_name()»
		«FOR i:0..<node.get_name.length + 11»-«ENDFOR»
		
		The conditions of the different ports are
		
		| Port condition | |
		|----------------|-|
		«FOR port: this.node.get_output_ports»
		|«port.get_variables.head»|«port.get_codition.toString.replace("\n", " ")»|
		«ENDFOR»
		
		The proof script is given by:
		«this.smt_file.write_markdown»
		'''
	}
	
	def write_file(IFileSystemAccess2 fase) {
		this.smt_file.write_file(fase)
	}
	
}