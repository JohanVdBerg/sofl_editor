package com.gitlab.johanvdberg.sofl_editor.consistency


import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.semantics.Module

class CdsaIo {
	public String in
	public String out
	public String formula
}

class MuCalculus {

	protected Module module

	new(Module module) {
		this.module = module

	}

	// test if multiple output after generated
	def eachInportMultipleOutput() {
		var out_list = new ArrayList<CdsaIo>
		for (cdsa_in : this.module.in_port_list) {
			var tmp = new CdsaIo
			tmp.in = cdsa_in.name
			tmp.out = 'all'
			tmp.formula = '''
			forall cip:PortId.val(cip == «tmp.in») && <true*.cdsa_input(«tmp.in»)>true =>  
				exists cop:PortId.exists cop2:PortId.val(cop != cop2) && 
					<true*.cdsa_output(cop).true*.cdsa_output(cop2)>true'''
			out_list.add(tmp)
		}
		return out_list
	}

	// make sure each input of the cdfd will results in an execution of a node
	def eachInportProcessExecute() {
		var out_list = new ArrayList<Pair<String, String>>
		for (in_port : this.module.in_port_list.map[it|it.name]) {
			out_list.add(new Pair(
				in_port,'''
				forall cip:PortId.val(cip == «in_port») => 
					exists nprt: PortId.exists nid:NodeId.
						[true*.cdsa_input(cip).true*.selected_port_generate(nid, nprt)]true'''
			))
		}
		return out_list
	}

	// test if a process can execute an infinite number of times
	def processFireInfinite() {
		var out_list = new ArrayList<Pair<String, String>>
		for (node_name : this.module.node_list.values.map[it.mcrl2_name].filter[it !== "NIp_Init"]) {
			out_list.add(new Pair(node_name, '''
			nu X.forall nid:NodeId. 
				val(nid == «node_name») => (
				<true*.fire(nid)>X
			)'''))
		}
		return out_list
	}

	// test that for each input put port and 
	// output port will generate data
	def cdsa_in_generate() {
		var out_list = new ArrayList<CdsaIo>
		for (cdsa_in : this.module.in_port_list) {
			var tmp = new CdsaIo
			tmp.in = cdsa_in.name
			tmp.formula = '''
			forall cip:PortId.val(cip == «tmp.in») && 
				<true*.cdsa_input(«tmp.in»)>true =>  
					exists cip:PortId. <true*.cdsa_output(cip)>true'''
			out_list.add(tmp)
		}
		return out_list
	}

	// determine the io relations of a cdfd	
	def cdsa_io_relation() {
		var out_list = new ArrayList<CdsaIo>
		for (cdsa_in : this.module.in_port_list) {
			for (cdsa_out : this.module.out_port_list) {
				var tmp = new CdsaIo
				tmp.in = cdsa_in.name
				tmp.out = cdsa_out.name
				tmp.formula = '''
				forall cip:PortId.val(cip == «tmp.in») && 
					<true*.cdsa_input(«tmp.in»)>true =>  
						exists cop:PortId.val(cop == «tmp.out») && <true*.cdsa_output(cop)>true'''
				out_list.add(tmp)
			}
		}
		return out_list
	}
	
	// determine if multiple outputs can be generated
	def cdfd_multi_output(){
		var out_list = new ArrayList<CdsaIo>
		for (cdsa_in : this.module.in_port_list) {
			for (cdsa_out : this.module.out_port_list) {
				var tmp = new CdsaIo
				tmp.in = cdsa_in.name
				tmp.out = cdsa_out.name
				tmp.formula = '''
				exists cip:PortId.val(cip == «tmp.in») && 
					<true*.cdsa_input(«tmp.in»)>true =>
						exists cop:PortId.<true*.cdsa_output(«tmp.out»).true*.cdsa_output(cop)>true
				'''
				out_list.add(tmp)
			}
		}
		return out_list		
	}

}