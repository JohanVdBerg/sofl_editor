package com.gitlab.johanvdberg.sofl_editor.semantics

import java.util.List
import java.util.ArrayList
import java.lang.RuntimeException
import java.util.Map
import java.util.Set
import java.util.HashMap
import java.util.HashSet
import org.eclipse.xtext.EcoreUtil2
import java.util.TreeSet
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.generator.ProcAlgGenerate
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NodeName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName

class Node implements NodeInterface {
	public String name
	public List<Port> in_ports
	public List<Port> out_ports
	public List<String> store_read
	public List<String> store_write
	public Map<Integer, HashSet<Integer>> io_relation;
	public Type type

	enum NodeType {
		Process,
		Condition,
		BroadCast,
		Other
	}

	new(ConditionStructure condition) {
		this.in_ports = new ArrayList<Port>
		this.out_ports = new ArrayList<Port>
		this.io_relation = new HashMap<Integer, HashSet<Integer>>
		this.store_read = new ArrayList<String>
		this.store_write = new ArrayList<String>

		this.name = condition.name
		this.in_ports.add(new Port(true, #[new VariableName(condition.in_variable)], 0, this))

		for (out : condition.out_list) {
			if (out.varname !== null) {
				this.out_ports.add(new Port(false, #[new VariableName(out.varname)], this.out_ports.length, this))
			}
		}

		// io relations
		var set = new HashSet<Integer>
		for (var j = 0; j < this.out_ports.length; j = j + 1) {
			set.add(j)
		}
		for (var i = 0; i < this.in_ports.length; i = i + 1) {
			this.io_relation.put(i, set)
		}

		this.type = Type.control

	}

	new(BroadcastStructure condition) {
		this.in_ports = new ArrayList<Port>
		this.out_ports = new ArrayList<Port>
		this.io_relation = new HashMap<Integer, HashSet<Integer>>
		this.store_read = new ArrayList<String>
		this.store_write = new ArrayList<String>

		this.name = condition.name
		if(condition.in_variable !== null){
			this.in_ports.add(new Port(true, #[new VariableName(condition.in_variable)], 0, this))		
		}else{
			this.in_ports.add(new Port(true, #[new VariableName(condition.in_connect)], 0, this))
		}

		var variable_list = new ArrayList<VariableName>
		for (out : condition.out_list) {
			variable_list.add(new VariableName(out))
		}
		for (out : condition.out_connect) {
			variable_list.add(new VariableName(out))
		}
		this.out_ports.add(new Port(false, variable_list, this.out_ports.length, this))
		// io relations
		var set = new HashSet<Integer>
		for (var j = 0; j < this.out_ports.length; j = j + 1) {
			set.add(j)
		}
		for (var i = 0; i < this.in_ports.length; i = i + 1) {
			this.io_relation.put(i, set)
		}

		this.type = Type.control
	}

	new(MergeStructure condition) {
		this.in_ports = new ArrayList<Port>
		this.out_ports = new ArrayList<Port>
		this.io_relation = new HashMap<Integer, HashSet<Integer>>
		this.store_read = new ArrayList<String>
		this.store_write = new ArrayList<String>
		this.name = condition.name

		var variable_list = new ArrayList<VariableName>
		for (out : condition.invar) {
			variable_list.add(new VariableName(out))
		}
		this.in_ports.add(new Port(true, variable_list, 0, this))
		this.out_ports.add(new Port(false, #[new VariableName(condition.out)], this.out_ports.length, this))

		// io relations
		var set = new HashSet<Integer>
		for (var j = 0; j < this.out_ports.length; j = j + 1) {
			set.add(j)
		}
		for (var i = 0; i < this.in_ports.length; i = i + 1) {
			this.io_relation.put(i, set)
		}

		this.type = Type.control

	}

	new(UnMergeStructure condition) {
		this.in_ports = new ArrayList<Port>
		this.out_ports = new ArrayList<Port>
		this.io_relation = new HashMap<Integer, HashSet<Integer>>
		this.store_read = new ArrayList<String>
		this.store_write = new ArrayList<String>

		this.name = condition.name
		this.in_ports.add(new Port(true, #[new VariableName(condition.invar)], 0, this))

		var variable_list = new ArrayList<VariableName>
		for (out : condition.out_list) {
			variable_list.add(new VariableName(out))
		}
		this.out_ports.add(new Port(false, variable_list, this.out_ports.length, this))

		// io relations
		var set = new HashSet<Integer>
		for (var j = 0; j < this.out_ports.length; j = j + 1) {
			set.add(j)
		}
		for (var i = 0; i < this.in_ports.length; i = i + 1) {
			this.io_relation.put(i, set)
		}

		this.type = Type.control

	}

	new(SoflProcess process) {
		this.in_ports = new ArrayList<Port>
		this.out_ports = new ArrayList<Port>
		this.io_relation = new HashMap<Integer, HashSet<Integer>>
		this.store_read = new ArrayList<String>
		this.store_write = new ArrayList<String>
		this.name = process.name

		if (process.in_ports !== null) {
			process.in_ports.ports.forEach [ port, port_index |
				val variable_list = new ArrayList<VariableName>
				for (varialbe_name : port.eAllContents.filter(SoflVariableName).toIterable) {
					variable_list.add(new VariableName(varialbe_name))
				}
				for (varialbe_name : port.eAllContents.filter(ConnectionName).toIterable) {
					variable_list.add(new VariableName(varialbe_name))
				}
				port.connections.forEach[variable_list.add(new VariableName(it))]
				/*if (port.connection !== null) {
				 * 	variable_list.add(Port.empty_variable_name(port.connection.name))
				 }*/
				this.in_ports.add(new Port(true, variable_list, this.in_ports.length, this))
			]
		}

		if (process.ext !== null) {
			for (stores : process.ext.variables) {
				if (stores.rd) {
					this.store_read.add(stores.varariable_name.name)
				} else {
					this.store_write.add(stores.varariable_name.name)
				}
			}
		}

		if (in_ports.length == 0) {
			in_ports.add(new Port(true, #[], 0, this))
		}

		if (process.out_ports !== null) {
			process.out_ports.ports.forEach [ port, port_index |
				val variable_list = new ArrayList<VariableName>
				for (varialbe_name : port.eAllContents.filter(SoflVariableName).toIterable) {
					variable_list.add(new VariableName(varialbe_name))
				}
				for (varialbe_name : port.eAllContents.filter(ConnectionName).toIterable) {
					variable_list.add(new VariableName(varialbe_name))
				}
				port.connections.forEach[variable_list.add(new VariableName(it))]
				/*if (port.connection !== null) {
				 * 	variable_list.add(Port.empty_variable_name(port.connection.name))
				 }*/
				this.out_ports.add(new Port(false, variable_list, this.out_ports.length, this))
			]
		}
		if (this.out_ports.length == 0) {
			this.out_ports.add(new Port(false, new ArrayList<VariableName>, 0, this))
		}
		// io relations
		var set = new HashSet<Integer>
		for (var j = 0; j < this.out_ports.length; j = j + 1) {
			set.add(j)
		}
		for (var i = 0; i < this.in_ports.length; i = i + 1) {
			this.io_relation.put(i, set)
		}
		this.type = Type.process
	}

	override get_containing_ports(com.gitlab.johanvdberg.sofl_editor.semantics.VariableName variable_name, Boolean from_input_port) {  
		if (from_input_port) {
			var currrent_port_itr = this.in_ports.filter([Port p|p.contain_variable(variable_name)])
			if (currrent_port_itr.length == 0) {
				System.out.println(this.in_ports.map[Port p|p.variables.map[v|v.variable_name]])
				System.out.println(
					"variables " + currrent_port_itr + " of length " + currrent_port_itr.length + " looking for " +
						variable_name + ' in node ' + this.toString)
				throw new RuntimeException("variables " + currrent_port_itr.length + " looking for " + variable_name + ' in node ' + this.toString)
			}
			return currrent_port_itr
		} else {
			var currrent_port_itr = this.out_ports.filter([Port p|p.contain_variable(variable_name)])
			if (currrent_port_itr.length == 0) {
				System.out.println(this.out_ports.map[Port p|p.variables.map[v|v.variable_name]])
				System.out.println(
					"variables " + currrent_port_itr + " of length " + currrent_port_itr.length + " looking for " +
						variable_name)
				throw new RuntimeException("variables " + currrent_port_itr.length + " looking for " + variable_name)
			}
			return currrent_port_itr
		}
	}

	def mcrl2_name() {
		return ProcAlgGenerate.node_name(this.get_node_name)
	}

	override get_node_name() {
		return this.name
	}

	override toString() {
		var ret = this.get_node_name + '['
		for(iport:this.in_ports){
			ret = ret + iport.toString + ','
		}
		ret = ret + ']['
		for(oport:this.out_ports){
			ret = ret + oport.toString + ','
		}
		ret = ret + ']'
		return ret
	}
	
	def empty_output_connected() {
		for (port : this.in_ports) {
			if (port.is_connected) {
				return true
			}
		}
		return false
	}

	def Set<Flow> get_all_in_flows() {
		var lst = this.in_ports.filter[it|it.get_flow_list().length > 0].map[it|it.get_flow_list].flatten
		var l = new TreeSet<Flow>
		l.addAll(lst)
		return l
	}

	def Set<Flow> get_all_out_flows() {
		var lst = this.out_ports.filter[it|it.get_flow_list().length > 0].map[it|it.get_flow_list].flatten
		var l = new TreeSet<Flow>
		l.addAll(lst)
		return l
	}

	def empty_input_connected() {
		for (port : this.out_ports) {
			if (port.is_connected) {
				return true
			}
		}
		return false
	}

	static def getNodeName(NodeName node) {
		if (node.cond !== null) {
			return node.cond.name
		} else if (node.proc !== null) {
			return node.proc.name
		} else if (node.is_behavior) {
			var module = EcoreUtil2.getContainerOfType(node, SoflModule)
			module.cdfd.name
		} else {
			null
		}
	}
	
	
		
	
}
