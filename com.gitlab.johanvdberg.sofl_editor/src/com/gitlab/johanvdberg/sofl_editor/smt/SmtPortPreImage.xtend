package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.ArrayList
import java.util.List
import java.util.TreeSet
import java.util.Map
import java.util.HashMap
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtFormula

class SmtPortPreImage {
	public List<SmtCondition> env_precondition_predicate_list
	public Map<String, TreeSet<SmtPort>> pre_image
	protected SmtPort current_port
	
	protected new(SmtPort current_port){
		this.env_precondition_predicate_list = new ArrayList<SmtCondition>
		this.pre_image = new HashMap<String, TreeSet<SmtPort>>
		this.current_port =current_port
	}

	def static create(SmtPort target_port){
		new SmtNodePortPreImage(target_port)
	}
	
	
	def get_environment(){
		this.env_precondition_predicate_list
	}
	
	def getMarkDownTable(){
		var text =
		'''
		Port:«this.current_port.get_name»
		«FOR i:0..<(this.current_port.get_name.length + 5)»=«ENDFOR»
		
		'''
		//for each module
		for(mod_name:this.pre_image.keySet){
			text = text +
			'''
			Module:«mod_name»
			«FOR i:0..<(mod_name.length + 7)»-«ENDFOR»

			'''
			//for each port
			text = text + 
			'''
			|Port Name      | Condition               | Types     |«FOR port : this.pre_image.get(mod_name)»
			|---------------|-------------------------|-----------|
			|«port.get_name»| «formula_table_entry(port.get_condition())»|«ENDFOR»
			'''
		}
		text
	}
	
	def formula_table_entry(SmtCondition formula){
		var varList = formula.variables.keySet.map[it| #[it, formula.variables.get(it)]]
		'''«formula.get_formula.smt» | «FOR i: varList» («i.get(0)»,«i.get(1)») «ENDFOR»'''
	}
	
	def formula_table_entry(Expression exp){
		val formula = new SmtFormula(exp)
		var varList = formula.variables.keySet.map[it| #[it, formula.variables.get(it)]]
		'''«formula.formula» | «FOR i: varList» («i.get(0)»,«i.get(1)») «ENDFOR»'''
	}
}