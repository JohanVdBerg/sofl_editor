package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.xtext.EcoreUtil2
import java.util.HashSet
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflDslFactory

class SmtModule {

	Map<String, SmtProcessNode> process_list
	Map<String, SmtStructureNode> structure_list
	String name
	//protected SoflModule syntax_module
	protected SmtModule parent
	public List<SmtCondition> invariants
	public SmtBehviorNode behavior

	public List<SmtCondition> initInvariants

	new(String name, SoflBehaviour diagram) {
		this.invariants = new ArrayList<SmtCondition>
		val module = EcoreUtil2.getContainerOfType(diagram, SoflModule)
		
		if(module.parent !== null){
			this.parent = new SmtModule(module.parent.name, module.parent.cdfd)			
		}else{
			this.parent = null
		}
		this.name = module.name
		this.process_list = new HashMap<String, SmtProcessNode>
		this.structure_list = new HashMap<String, SmtStructureNode>

		// get the init invariant
		if (module?.init?.inv_list !== null) {
			this.initInvariants = module.init.inv_list.map[SmtCondition::create(it)]
		} else {
			this.initInvariants = #[]
		}
		// get the invariant equations
		if (module?.invariant?.invariant !== null) {
			for (i : module.invariant.invariant) {
				this.invariants.add(SmtCondition::create(i))
			}
		}

		this.behavior = SmtNode::create(diagram, this)
		// this.node_list.put(behavior.name, behavior)
		// for each flow
		for (flow : diagram.flow) {
			var src = if (flow.source.node.broadcast !== null) {
					add_node(flow.source.node.broadcast)
				} else if (flow.source.node.proc !== null) {
					add_node(flow.source.node.proc)
				} else if (flow.source.node.cond !== null) {
					add_node(flow.source.node.cond)
				} else if (flow.source.node.merge !== null) {
					add_node(flow.source.node.merge)
				} else if (flow.source.node.unmerge !== null) {
					add_node(flow.source.node.unmerge)
				} else if (flow.source.node.is_behavior) {
					this.behavior
				}

			var dest = if (flow.destination.node.broadcast !== null) {
					add_node(flow.destination.node.broadcast)
				} else if (flow.destination.node.proc !== null) {
					add_node(flow.destination.node.proc)
				} else if (flow.destination.node.cond !== null) {
					add_node(flow.destination.node.cond)
				} else if (flow.destination.node.merge !== null) {
					add_node(flow.destination.node.merge)
				} else if (flow.destination.node.unmerge !== null) {
					add_node(flow.destination.node.unmerge)
				} else if (flow.destination.node.is_behavior) {
					this.behavior
				}

			var in_ports = get_ports_containing(src.get_source_ports, flow.source.variable_name.name)
			var out_ports = get_ports_containing(dest.get_destination_ports, flow.destination.variable_name.name)
			for (iport : in_ports) {
				for (oport : out_ports) {
					iport.addConectedPort(oport)
					oport.addConectedPort(iport)
				}
			}
		}

		// for each shadow flow
		for (flow : diagram.shadow_flow) {
			var src = if (flow.source.node.broadcast !== null) {
					add_node(flow.source.node.broadcast)
				} else if (flow.source.node.proc !== null) {
					add_node(flow.source.node.proc)
				} else if (flow.source.node.cond !== null) {
					add_node(flow.source.node.cond)
				} else if (flow.source.node.merge !== null) {
					add_node(flow.source.node.merge)
				} else if (flow.source.node.unmerge !== null) {
					add_node(flow.source.node.unmerge)
				} else if (flow.source.node.is_behavior) {
					this.behavior
				}

			var dest = if (flow.destination.node.broadcast !== null) {
					add_node(flow.destination.node.broadcast)
				} else if (flow.destination.node.proc !== null) {
					add_node(flow.destination.node.proc)
				} else if (flow.destination.node.cond !== null) {
					add_node(flow.destination.node.cond)
				} else if (flow.destination.node.merge !== null) {
					add_node(flow.destination.node.merge)
				} else if (flow.destination.node.unmerge !== null) {
					add_node(flow.destination.node.unmerge)
				} else if (flow.destination.node.is_behavior) {
					this.behavior
				}

			var in_ports = get_ports_containing(src.get_source_ports, flow.source.connected_name.name)
			var out_ports = get_ports_containing(dest.get_destination_ports, flow.destination.connected_name.name)
			for (iport : in_ports) {
				for (oport : out_ports) {
					iport.addConectedPort(oport)
					oport.addConectedPort(iport)
				}
			}
		}
		// compute relationship between ports
		for (node : this.allNodes) {
			node.computeProofObligations
		}
	}

	def get_refined_process(){
		'process name to be refined'
	}
	
	def get_parent_name(){
		this.parent.name
	}
	def have_parent() {
		return this.parent !== null
	}

	def getName() {
		this.name
	}

	def get_process_being_refined() {
		var canidate_list = this.parent.process_list.filter[name, process| process.decomposed_module_name == this.name].values
		if (canidate_list.length != 1) {
			throw new RuntimeException('''Not allowed «canidate_list.length»''')
		}
		canidate_list.head
	}

	def List<SmtCondition> getInitInvaraints() {
		this.initInvariants
	}

	/*
	 * protected def add_node(Behavior node){
	 * 	if(this.node_list.containsKey(node.name)){
	 * 		return this.node_list.get(node.name)
	 * 	}else{
	 * 		var n = SmtNode::create(node, this)
	 * 		this.node_list.put(node.name, n)
	 * 		return n
	 * 	}
	 }*/
	protected def add_node(SoflProcess node) {
		if (this.process_list.containsKey(node.name)) {
			return this.process_list.get(node.name)
		} else {
			var n = SmtNode::create(node, this)
			this.process_list.put(node.name, n)
			return n
		}
	}

	def static and_op(Expression left, Expression right) {
		var exp = SoflDslFactory.eINSTANCE.createExpAnd
		exp.left = left
		exp.right = right
		return exp
	}

	def static conjunction(List<Expression> exp_lst) {
		var current = exp_lst.head
		var lst = exp_lst.tail
		while (lst.length > 0) {
			current = and_op(current, lst.head)
			lst = lst.tail
		}
		return current
	}

	def getCondition(SoflProcess process, int port_index, boolean input) {
		var lst = if (input) {
				process.state.state_list.filter[it|it.in_port_id == port_index].map[it|it.pre.condition]
			} else
				{
					process.state.state_list.filter[it|it.out_port_id == port_index].map[it|it.post.condition]
				}.filter[it !== null]
		var current = conjunction(lst.toList)
		SmtCondition::create(current)
		SmtPort
	}

	def getAllNodes() {
		var node_set = new HashSet<SmtNode>
		node_set.addAll(this.getAllProcesses)
		node_set.addAll(this.getControlNodes)
		node_set.add(this.behavior)
		node_set
	}

	def getAllProcesses() {
		this.process_list.values
	}

	def getControlNodes() {
		this.structure_list.values
	}

	protected def add_node(ConditionStructure node) {
		if (this.structure_list.containsKey(node.name)) {
			return this.structure_list.get(node.name)
		} else {
			var n = SmtNode::create(node, this)
			this.structure_list.put(node.name, n)
			return n
		}
	}

	protected def add_node(BroadcastStructure node) {
		if (node === null) {
			throw new RuntimeException("Null")
		}
		if (this.structure_list.containsKey(node.name)) {
			return this.structure_list.get(node.name)
		} else {
			var n = SmtNode::create(node, this)
			this.structure_list.put(node.name, n)
			return n
		}
	}

	protected def add_node(MergeStructure node) {
		if (this.structure_list.containsKey(node.name)) {
			return this.structure_list.get(node.name)
		} else {
			var n = SmtNode::create(node, this)
			this.structure_list.put(node.name, n)
			return n
		}
	}

	protected def add_node(UnMergeStructure node) {
		if (this.structure_list.containsKey(node.name)) {
			return this.structure_list.get(node.name)
		} else {
			var n = SmtNode::create(node, this)
			this.structure_list.put(node.name, n)
			return n
		}
	}

	def get_ports_containing(List<SmtPortInNode> ports, String varaible) {
		var out = new ArrayList<SmtPortInNode>
		for (i : ports) {
			if (i.contains_variable(varaible)) {
				out.add(i)
			}
		}
		return out
	}

	def get_parent(){
		this.parent
	}
	
	def get_parent_invaraints() {
		if (this?.parent !== null) {
			this.parent.invariants
		} else {
			#[SmtCondition::create(true)]
		}
	}

	def get_behavior() {
		this.behavior
	}

}