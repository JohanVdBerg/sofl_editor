package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

class ProofObligationImply extends ProofObligation {
	public SmtCondition P
	public SmtCondition Q
	public SmtCondition imply_condition
	public SmtLogicScope imply_scope
	
	def assign_condition(List<SmtCondition> p_list, List<SmtCondition> q_list){
		this.P = SmtCondition::conjunction(p_list)
		this.Q = SmtCondition::conjunction(q_list)
		
		this.imply_scope = new SmtLogicScope
		for(v: this.P.variables.keySet){
			imply_scope.add(v, this.P.variables.get(v))
		}
		if(this.imply_scope.varaible_count > 0){
			this.imply_condition = SmtCondition::forall(imply_scope, SmtCondition.imply(this.P, this.Q))		
		}else{
			this.imply_condition = SmtCondition.imply(this.P, this.Q)
		}
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents
		
		for(v: this.imply_condition.variables.keySet){
			file_data.add_variable(v, this.imply_condition.variables.get(v))					
		}
		
		//prove the implication
		file_data.add_push
		file_data.add_assert_formula(this.imply_condition.get_formula.smt)
		file_data.add_print("Check if environment-->condition is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		//prove the environment is satisfiable
		file_data.add_push
		file_data.add_assert_formula(this.P.get_formula.smt)
		file_data.add_print("Check if the environment is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
}
