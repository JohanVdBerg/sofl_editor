package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.SmtPortInNode
import java.util.List

class ProofObligationPredicateTransform extends ProofObligationImply {
	public List<SmtCondition> start_invariants
	public SmtCondition pre_condition
	public SmtCondition post_condition	
	public int in_port_index
	public int out_port_index
	public List<SmtCondition> end_invariants
	
	
	def static create(List<SmtCondition> start_invariants, SmtPortInNode start_port, SmtPortInNode end_port,  List<SmtCondition> end_invariants){
		new ProofObligationPredicateTransform(start_invariants, start_port, end_port, end_invariants)						
	}
	
	protected new (List<SmtCondition> start_invariants, SmtPortInNode in_port, SmtPortInNode out_port, List<SmtCondition> end_invariants){
		
		this.obligation_name = '''in«in_port.index + 1» out«out_port.index + 1»'''
		this.start_invariants = start_invariants
		this.end_invariants = end_invariants
		
		this.in_port_index = in_port.index
		this.out_port_index = out_port.index
		val trans = out_port.parentNode.io_conditions.filter[it| it.in_port == in_port.index && it.out_port == out_port.index]
		
		if(trans.length != 1){
			throw new RuntimeException('only one pre/post condition is allowed')
		}
		this.post_condition = trans.head.post_condition
		this.pre_condition = trans.head.pre_condition
		super.assign_condition((this.start_invariants.toList + #[this.pre_condition]).toList, (this.end_invariants.toList + #[this.post_condition]).toList)
	}
	
	

	
}