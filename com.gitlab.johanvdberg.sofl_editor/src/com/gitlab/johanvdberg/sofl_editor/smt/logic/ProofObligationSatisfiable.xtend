package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

class ProofObligationSatisfiable extends ProofObligation {
	
	public List<SmtCondition> condition
	new(String name, List<SmtCondition> conditions) {
		this.obligation_name = name
		this.condition = new ArrayList<SmtCondition>
		this.condition.addAll(conditions)		
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents
		
		var env_condition = SmtCondition.conjunction(this.condition)
		var varialbes = env_condition.variables
		for(v: varialbes.keySet){
			file_data.add_variable(v, varialbes.get(v))					
		}
		//prove the environment is satisfiable
		file_data.add_push
		file_data.add_assert_formula(env_condition.get_formula.smt)
		file_data.add_print("Check if the environment is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
}
