package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

class ProofObligationCondition extends ProofObligation {
	new(List<SmtCondition> condition_list){
		this.environment.addAll(condition_list)
	}
	
	override String smt_proof(){
		//var tmp = ConstantStore.store
		var file_data = new SmtFileContents
		
		
		for(out_index:0..<this.environment.length - 1){
			for(inner_index:out_index+1..< this.environment.length){
				var cond = SmtCondition::conjunction(#[this.environment.get(out_index), this.environment.get(inner_index)])
				file_data.add_push
				file_data.add_assert_formula(cond.get_formula.smt)
				file_data.add_print('''This formula must not be satisfiable port «out_index» and «inner_index»''')
				file_data.add_sat_check
				file_data.add_pop
			}
		}
		file_data.contents
	}
}
