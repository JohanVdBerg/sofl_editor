package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.Set
import java.util.HashSet
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase

class SmtLogicScope extends Logic{
	public Set<SmtLogicScopePair> pairs
	
	
	new(){
		super(Operation.SCOPE)
		this.pairs = new HashSet<SmtLogicScopePair>
	}
	
	
	def varaible_count(){
		this.pairs.length
	}
	
	def add(String variable, TypeBase type){
		this.pairs.add(new SmtLogicScopePair(variable, type))
	}
	
	def contain(String variable, TypeBase type){
		this.pairs.filter[it.variable == variable && it.type == type].length > 0
	}
	
	def contain(String variable){
		this.pairs.filter[it.variable == variable].length > 0
	}
	
	override smt(){
		'''( «FOR term:this.pairs» («term.variable.smt» «Logic::smt(term.type)» ) «ENDFOR»)'''
	}
}
