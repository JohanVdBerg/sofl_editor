package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

/*!
 * A prove obligation consist of and environment and statement (S) to be proven
 * The environment must defined the states in which the statement S must be valid.
 */
class ProofObligation {
	public List<SmtCondition> environment
	List<SmtCondition> invariants
	//SmtCondition port_obligation
	SmtCondition obligation
	public String obligation_name
	
	
	protected new(){
		this.environment = new ArrayList<SmtCondition>
	}
	
	protected new(SmtCondition invariant, SmtCondition environment, SmtCondition obligation){
		this.environment = new ArrayList<SmtCondition>
		this.invariants = new ArrayList<SmtCondition>		
		this.environment.add(environment)
		this.invariants.add(invariant)
		//this.port_obligation = obligation
		this.obligation = SmtCondition::conjunction(#[invariant, obligation])
	}
	
	protected new(List<SmtCondition> invariant,List<SmtCondition> environment_list, SmtCondition obligation){
		this.environment = new ArrayList<SmtCondition>
		this.invariants = new ArrayList<SmtCondition>
		this.environment.addAll(environment_list)
		this.invariants.addAll(invariant)
		//this.port_obligation = obligation
		this.obligation = SmtCondition::conjunction(#[SmtCondition::conjunction(invariant), obligation])
	}
	
	
	
	def String smt_proof(){
		var file_data = new SmtFileContents
		
		var env_condition = SmtCondition.conjunction(#[SmtCondition.conjunction(this.environment),SmtCondition.conjunction(this.invariants)])
		var imply_condition = SmtCondition.imply(env_condition, SmtCondition.conjunction(#[this.obligation,SmtCondition.conjunction(this.invariants)]))
		for(v: env_condition.variables.keySet){
			file_data.add_variable(v, imply_condition.variables.get(v))					
		}
		
		//prove the implication
		file_data.add_push
		file_data.add_assert_formula(imply_condition.get_formula.smt)
		file_data.add_print("Check if environment-->condition is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		//prove the environment is satisfiable
		file_data.add_push
		file_data.add_assert_formula(env_condition.get_formula.smt)
		file_data.add_print("Check if the environment is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents
	}
}

