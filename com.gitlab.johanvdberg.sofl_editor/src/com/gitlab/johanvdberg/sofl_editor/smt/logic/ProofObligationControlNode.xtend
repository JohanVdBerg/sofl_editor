package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.SmtFile
import com.gitlab.johanvdberg.sofl_editor.smt.SmtStructureNode

class ProofObligationControlNode extends ProofObligationImply{
		
	public SmtFile contrain_proof
		
	new (SmtStructureNode node){
		this.contrain_proof = new SmtFile('''node_control_«node.name».smt2''', SmtFile.ProofType::Constrain, "")
	}
}
