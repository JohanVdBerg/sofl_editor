package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.List
import org.eclipse.emf.ecore.EObject
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.types.VariableType

class SmtPortStructureNode extends SmtPortInNode{
	
	new(SmtNode parent, int index, List<VariableType> varaible_list, EObject syntax_port){
		super()
		this.node_parent = parent
		this.index = index
		this.variable_list.addAll(varaible_list)
		//this.input = input	
		this.syntax_port = syntax_port
		
		
	}
	
	override get_codition() {
		var lst = this.get_connected_ports.map[it.condition]
		SmtCondition.conjunction(lst)
	}
}