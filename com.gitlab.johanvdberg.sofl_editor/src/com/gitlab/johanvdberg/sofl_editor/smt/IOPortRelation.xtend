package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression

class IOPortRelation {
	public int in_port
	public int out_port
	public SmtCondition pre_condition
	public SmtCondition post_condition
	
	
	
	def static create(int iport, int oport, Expression pre_condition, Expression post_condition) {
		new IOPortRelation(iport, oport, SmtCondition::create(pre_condition), SmtCondition::create(post_condition))
	}
	
	def static create(int iport, int oport, SmtCondition precondition, SmtCondition postcondition) {
		new IOPortRelation(iport, oport, precondition, postcondition)
	}
	
	new(int iport, int oport, SmtCondition precondition, SmtCondition postcondition) {
		this.in_port = iport
		this.out_port = oport
		this.pre_condition = precondition
		this.post_condition = postcondition		 	
	}
}