package com.gitlab.johanvdberg.sofl_editor.smt.file

import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import java.util.Comparator

class DeclareVariable implements Comparable<DeclareVariable>, Comparator<DeclareVariable> {
	String name
	String type
	
	new(String name, String type){
		this.name = name
		this.type = type
	}
	
	new(String name, TypeBase type){
		this.name = name
		this.type = SmtCondition::smt_type(type)
	}
	
	override compareTo(DeclareVariable o) {
		this.name.compareTo(o.name)
	}
	
	def smt(){
		'''(declare-const «name» «type»)'''
	}
	
	override compare(DeclareVariable o1, DeclareVariable o2) {
		o1.compareTo(o2)
	}
	
}
