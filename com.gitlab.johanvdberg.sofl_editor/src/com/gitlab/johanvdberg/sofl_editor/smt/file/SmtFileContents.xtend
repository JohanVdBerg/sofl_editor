package com.gitlab.johanvdberg.sofl_editor.smt.file

import com.gitlab.johanvdberg.sofl_editor.types.BooleanType
import com.gitlab.johanvdberg.sofl_editor.types.Natural0Type
import com.gitlab.johanvdberg.sofl_editor.types.NaturalType
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import java.util.HashMap
import java.util.Map
import java.util.Set
import java.util.TreeSet

class SmtFileContents {
	String contents_body
	String contents_head
	
	Set<String> constant_used
	Set<DeclareVariable> declareVariables
	new(){
		this.constant_used = new TreeSet<String> 
		this.declareVariables = new TreeSet<DeclareVariable>
		this.contents_head = 
		'''
		(set-option :print-success false)
		(set-logic ALL)
		'''
	}

	protected def add_if_constant(String name){
		if(ConstantStore::containVariable(name)){
			this.constant_used.add(name)
		}
		return false
	}
	
	def add_variable(String variable_name, TypeBase type){
		
		if(add_if_constant(variable_name)){
			return // the  variable is a constant and will be define in the file later
		}
		this.declareVariables.add(new DeclareVariable(variable_name, type))
		
		if(type instanceof BooleanType){
			this.add_assert_formula('''(or (= «variable_name» 0) (= «variable_name» 1) )''')
		}else if(type instanceof Natural0Type){
			this.add_assert_formula('''(>= «variable_name» 0)''')
		}else if(type instanceof NaturalType){
			this.add_assert_formula('''(> «variable_name» 0)''')
		}
	}
	
	def add_print(String str){
		this.contents_body = 
		'''
		«this.contents_body»
		(echo "«str»")
		'''
	}
	
	def add_assert_formula(String formula){
		
		this.contents_body = 
		'''
		«this.contents_body»
		(assert «formula»)
		'''
	}
	
	def add_assert_formula(Map<String, TypeBase> variables_p, String formula){
		var variables = new HashMap<String, TypeBase>
		for(k: variables_p.keySet){
			if(!this.add_if_constant(k)){
				variables.put(k, variables_p.get(k))
			}
		}
		if( variables.size > 0){
			this.contents_body = 
			'''
			«this.contents_body»
			(assert (forall («FOR v: variables.keySet» («v» («variables.get(v)»)) «ENDFOR») «formula»))
			'''
		}else{
			this.add_assert_formula(formula)
		}
	}
	
	def add_pop(){
		this.contents_body =
		'''
		«this.contents_body»
		(pop)		
		'''
	}
	
	def add_push(){
			this.contents_body =
		'''
		«this.contents_body»
		(push)		
		'''
	}
	def add_sat_check(){
		this.contents_body =
		'''
		«this.contents_body»
		(check-sat)
		'''
	}
	
	def String getContents(){
		for(c: ConstantStore::constants){
			this.declareVariables.add(new DeclareVariable(c.name, c.type))
		}
		'''
		«this.contents_head»
		
		«FOR current: this.declareVariables»
		«current.smt»
		«ENDFOR»
		
		«ConstantStore::smt(this.constant_used)»
		
		
		«this.contents_body»
		'''
	}
}