package com.gitlab.johanvdberg.sofl_editor.types



class TypeBase implements Comparable<TypeBase>{
	public static String FUNCTION_TYPE="function_type"
	public static String PROCESS_TYPE="process_type"
	public static String BEHAVIOR_TYPE="behavior_type"
	public static String BROADCAST_TYPE="braodcast_type"
	public static String CONDITION_TYPE="condtition_type"
	public static String MERGE_TYPE="merge_type"
	public static String UN_MERGE_TYPE="un_merge_type"
	public static String MAP_TYPE="map_type"
	public static String SET_TYPE="set_type"
	public static String SET_OF_POSIIBLE_TYPE="set_of_possible_types"
	public static String SEQ_TYPE="seq_type"
	public static String COMP_TYPE="composed_type"
	public static String ENUM_TYPE="enumerate_type"
	public static String BOOL_TYPE="boolean_type"
	public static String STRING_TYPE="string_type"
	public static String INT_TYPE="INT_type"
	public static String NAT_TYPE="NAT_type"
	public static String NAT0_TYPE="NAT0_type"
	public static String CHAR_TYPE="CHAR_type"
	public static String REAL_TYPE="REAL_type"
	public static String CONNECTION_TYPE="CONNECTION_type"
	public static String EMPTY_TYPE="EMPTY_type"
	
	String current_type
	new(String type){
		this.current_type = type
	}
	
	override equals(Object o){
		if(o instanceof String){
			return this.typeClass == o
		}else if(o instanceof PossibleSetOfTypes){
			return o.equals(this)
		}else if (o instanceof TypeBase){
			return this.current_type == o.current_type
		}
		return super.equals(o)
	}
	def operator_notEquals(Object oo){
		!(this.equals(oo))
	}
	def String getTypeClass(){
		this.current_type
	}
	
	def is_boolean(){
		return TypeBase::BOOL_TYPE == this.typeClass
	}
	
	def is_discrete_number(){
		switch this.typeClass{
			case NAT0_TYPE:
				return true
			case NAT_TYPE:
				return true
			case INT_TYPE:
				return true
		}
		return false
	}
	
	def is_number(){
		return this.is_discrete_number() || this.getTypeClass() == TypeBase::REAL_TYPE
	}
	
	def is_set(){
		this.typeClass == TypeBase::SET_TYPE
	}
	
	def is_composed_type(){
		this.typeClass == TypeBase::COMP_TYPE
	}
	def is_map_type(){
		return this.typeClass == TypeBase::MAP_TYPE
	}
	
	def is_sequnce(){
		return this.typeClass == TypeBase::SEQ_TYPE
	}
	def is_error(){
		return false
	}
	
	override toString(){
		this.typeClass
	}
	
	override compareTo(TypeBase o) {
		this.getTypeClass().compareTo(o.typeClass)
	}
}
