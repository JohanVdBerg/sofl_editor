package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.evaluate.Evaluate
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ArgumentsModify
import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.CaseExpression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ComposedElement
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Constant
import com.gitlab.johanvdberg.sofl_editor.soflDsl.EnumerateValue
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAdd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAnd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpBool
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpDiv
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEnum
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEquiv
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrt
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrtEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpImply
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpInSet
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpIntDiv
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLessEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMinus
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMult
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNeg
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNeq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNot
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNotIn
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNumber
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpOr
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpPower
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpScopeModify
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpString
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpressionSequence
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExternalVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.FunctionReferenceModifiers
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MapSequnce
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NewType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NewVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NodeName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ParamSingleTypeDecl
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PreDefinedFunctions
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflFunction
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflNumber
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.TypeIdentifier
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.VariableDeclare
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName
import java.util.ArrayList
import java.util.List
import org.eclipse.xtext.EcoreUtil2
import com.gitlab.johanvdberg.sofl_editor.smt.file.ConstantStore

class ExpressionType {

	def static TypeBase getType(Expression exp) {
		if(exp === null){
			new ErrorType('Type could not be computed error')
		}
		if (exp instanceof ExpOr) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_boolean && right.is_boolean) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» or «right»''')
			}
		} else if (exp instanceof ExpAnd) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_boolean && right.is_boolean) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» and «right»''')
			}
		} else if (exp instanceof ExpImply) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_boolean && right.is_boolean) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» => «right»''')
			}
		} else if (exp instanceof ExpEquiv) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_boolean && right.is_boolean) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» <=> «right»''')
			}
		} else if (exp instanceof ExpEq) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if ((left == right) || (left.is_number && right.is_number)) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» == «right»''')
			}
		} else if (exp instanceof ExpNeq) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if ((left == right) || (left.is_number && right.is_number)) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» <> «right»''')
			}
		} else if (exp instanceof ExpInSet) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (right instanceof SetType) {
				if (left == right.getElementType) {
					return new BooleanType
				} else {
					return new ErrorType('''«left» inset «right»''')
				}
			} else {
				return new ErrorType('''need to be set «right»''')
			}
		} else if (exp instanceof ExpNotIn) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (right instanceof SetType) {
				if (left == right.getElementType) {
					return new BooleanType
				} else {
					return new ErrorType('''«left» notin «right»''')
				}
			} else {
				return new ErrorType('''need to be set «right»''')
			}
		} else if (exp instanceof ExpLess) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» < «right»''')
			}
		} else if (exp instanceof ExpGrt) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» > «right»''')
			}
		} else if (exp instanceof ExpLessEq) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» <= «right»''')
			}
		} else if (exp instanceof ExpGrtEq) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new BooleanType
			} else {
				return new ErrorType('''«left» >= «right»''')
			}
		} else if (exp instanceof ExpAdd) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if( left instanceof NumericType && right instanceof NumericType){
				return (left as NumericType).get_larger_of_the_two(right as NumericType)
			} else {
				return new ErrorType('''«left» + «right»''')
			}
		} else if (exp instanceof ExpMinus) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if( left instanceof NumericType && right instanceof NumericType){
				return (left as NumericType).get_larger_of_the_two(right as NumericType)
			} else {
				return new ErrorType('''«left» - «right»''')
			}
		} else if (exp instanceof ExpMult) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left == right) {
				return left
			} else if (left instanceof Natural0Type) {
				return right
			} else if (left instanceof NaturalType) {
				return right
			} else if (left instanceof IntegerType) {
				return right
			} else if (left.is_number && right.is_number) {
				return new RealType
			} else {
				return new ErrorType('''«left» * «right»''')
			}
		} else if (exp instanceof ExpPower) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_discrete_number && (right instanceof Natural0Type || right instanceof NaturalType)) {
				return left
			} else if (left.is_number && right.is_number) {
				return new RealType
			} else {
				return new ErrorType('''«left» ** «right»''')
			}
		} else if (exp instanceof ExpDiv) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new RealType
			} else {
				return new ErrorType('''«left» / «right»''')
			}
		} else if (exp instanceof ExpIntDiv) {
			var left = getType(exp.left)
			var right = getType(exp.right)
			if (left.is_number && right.is_number) {
				return new IntegerType
			} else {
				return new ErrorType('''«left» div «right»''')
			}
		} else if (exp instanceof ExpNot) {
			var type = getType(exp.expression)
			if (type.is_boolean) {
				return new BooleanType
			} else {
				return new ErrorType('''no «type»''')
			}
		} else if (exp instanceof ExpString) {
			return new SeqType(new CharacterType)
		} else if (exp instanceof ExpBool) {
			return new BooleanType
		} else if (exp instanceof ExpEnum) {
			return getType(exp.enumerate)
		} else if (exp instanceof ExpNumber) {
			return getType(exp.number)
		} else if (exp instanceof ExpNeg) {
			var type = getType(exp.expression)
			if (type.is_discrete_number) {
				new IntegerType
			} else if (type.is_number) {
				type
			} else {
				new ErrorType('''negative of type «type» not supported''')
			}
		} else if (exp instanceof ExpScopeModify) {
			var type = if (exp.is_curly) {
					if (exp.map_seq !== null) {
						getMapType(exp.map_seq)
					} else if (exp.set_compress !== null) {
						new SetType(getType(exp.set_compress.new_var.list.head.type))
					} else if (exp.expression_seq !== null) {
						val type_list = exp.expression_seq.exp_list.map[it|getType(it)]
						if (type_list.forall[it|it == type_list.head]) {
							new SetType(type_list.head)
						} else {
							new ErrorType('''{«type_list»}''')
						}
					}
				} else if (exp.is_square) {
					if (exp.exp !== null) {
						val type = getType(exp.exp)
						if (exp.exp_list !== null) {
							val type_list = exp.exp_list.map[it|getType(it)]
							if (type_list.forall[it|it == type]) {
								new SeqType(type)
							} else {
								new ErrorType('''[«type», «type_list»]''')
							}
						} else if (exp.last !== null) {
							var second = getType(exp.last)
							if (type == second) {
								new SeqType(type)
							} else {
								new ErrorType('''[«type»,...,«second»]''')
							}
						} else if (exp.var_list !== null) {
							new SeqType(type)
						} else {
							new ErrorType('getType(Expression)2')
						}
					} else {
						if (exp.list_type !== null) {
							var tmp_tmp = getType(exp.list_type)
							if (tmp_tmp instanceof SeqType) {
								tmp_tmp
							} else {
								new ErrorType('''type «tmp_tmp» must be a sequence type''')
							}
						} else {
							new ErrorType('Type of empty sequence must be defined explicitly')
						}
					}
				} else if (exp.is_case) {
					val list = exp.case_expression.case_item.map[it|getType(it.result)]
					val root = if (exp?.case_expression?.expression !== null) {
							getType(exp.case_expression.expression)
						} else {
							list.head
						}
					if (list.forall[it|it == root]) {
						root
					} else {
						new ErrorType('getType(Expression)5')
					}
				} else if (exp.is_let) {
					getType(exp.expression)
				} else if (exp.is_if) {
					var cond_expression = getType(exp.cond_expression)
					if (cond_expression.is_boolean) {
						var then_expression = getType(exp.then_expression)
						var else_expression = getType(exp.else_expression)
						if (then_expression == else_expression) {
							then_expression
						} else {
							var str = '''if then «then_expression» else «else_expression»'''
							new ErrorType(str)
						}
					} else {
						new ErrorType('''if «cond_expression»''')
					}
				} else if (exp.variable !== null) {
					if (exp.arguments !== null) {
						var module = EcoreUtil2.getContainerOfType(exp.variable, SoflModule)
						var function = getFunction(exp.variable, module)
						if (function === null) {
							getVariableType(exp.variable, exp.arguments)
						} else {
							getVariableType(exp.variable, exp.arguments, function)
						}
					} else {
						getVariableType(exp.variable)
					}
				} else if (exp.isIs_forall || exp.is_exists || exp.is_grouping) {
					getType(exp.expression)
				} else if (exp.function_apply !== null) {
					getType(exp.function_apply)
				} else {
					new ErrorType('getType(Expression)3')
				}
			if (type === null) {
				throw new RuntimeException("null pointer")
			}
			if (exp?.modifiers !== null && !type.is_error) {
				for (mod : exp.modifiers) {
					type = apply_modifier(type, mod)
				}
			}
			return type
		} else {
			return new ErrorType('getType(Expression)1')
		}
	}

	def static TypeBase getType(SoflType exp) {
		return Type::getType(exp)
	}

	def static getMapType(MapSequnce data) {
		val dom = data.list.map[getType(it.src)].toSet
		val ran = data.list.map[getType(it.dest)].toSet
		if (!dom.forall[it == dom.head]) {
			return new ErrorType('''domain of map can any one of the types «dom»''')
		}
		if (!ran.forall[it == ran.head]) {
			return new ErrorType('''range of map can any one of the types «ran»''')
		}
		new MapType(dom.head, ran.head)
	}

	def static TypeBase getType(SoflVariableName name) {
		getVariableType(name)
	}

	def static TypeBase getVariableType(SoflVariableName name) {
		// test if the type is defined in an expression
		var expression_temp = EcoreUtil2.getContainerOfType(name, ExpScopeModify)
		while (expression_temp !== null) {
			if (expression_temp.is_let) {
				for (current : expression_temp.pattern_list) {
					for (var i = 0; i < current.id_list.length; i = i + 1) {
						if (current.id_list.get(i).name == name.name) {
							return getType(current.expression)
						}
					}
				}
			} else if (expression_temp.is_forall || expression_temp.is_exists) {
				for (dec : expression_temp.bindings.declare) {
					if (dec.identifier.exists[it|it.name == name.name]) {
						return getType(dec.type)
					}
				}
			} else if (expression_temp?.set_compress?.new_var !== null) {
				var current_var = expression_temp.set_compress.new_var
				for (current : current_var.list) {
					if (current.identifier.exists[it.name == name.name]) {
						return getType(current.type)
					}
				}
			}
			expression_temp = EcoreUtil2.getContainerOfType(expression_temp.eContainer, ExpScopeModify)
		}
		//
		var condition = EcoreUtil2.getContainerOfType(name, ConditionStructure)
		if (condition !== null) {
			return getVariableType(name, condition)
		}
		var merge = EcoreUtil2.getContainerOfType(name, MergeStructure)
		if (merge !== null) {
			return getVariableType(name, merge)
		}
		var unmerge = EcoreUtil2.getContainerOfType(name, UnMergeStructure)
		if (unmerge !== null) {
			return getVariableType(name, unmerge)
		}
		var broadcast = EcoreUtil2.getContainerOfType(name, BroadcastStructure)
		if (broadcast !== null) {
			return getVariableType(name, broadcast)
		}

		// test if in process
		var is_process = EcoreUtil2.getContainerOfType(name, SoflProcess)
		if (is_process !== null) {
			return getVariableType(name, is_process)
		}
		var is_behavior = EcoreUtil2.getContainerOfType(name, SoflBehaviour)
		if (is_behavior !== null) {
			return getVariableType(name, is_behavior)
		}
		// the other case is that it is defined in a module
		var module = EcoreUtil2.getContainerOfType(name, SoflModule)
		if (module !== null) {
			return getVariableType(name, module)
		}
		var cont = name.eContainer
		if (cont instanceof NewVariable) {
			return getType(cont.type)
		} else if (cont instanceof VariableDeclare) {
			return getType(cont.type)
		} else if (cont instanceof ParamSingleTypeDecl) {
			return getType(cont.type)
		} else if (cont instanceof Constant) {
			return getType(cont.expression)
		}
		var const_def = EcoreUtil2.getContainerOfType(name, Constant)
		if (const_def !== null) {
			return getType(const_def.expression)
		}
		for (current : module.const_declare.const_list) {
			if (name.name == current.name.name) {
				var t_type = getType(current.expression)
					var t_value = Evaluate::evaluate(current.expression)
					ConstantStore::add(current.name.name, t_type, t_value)
				return t_type
			}
		}
		return new ErrorType('getType(VariableName name) {')
	}

	def static TypeBase getVariableType(SoflVariableName name, BroadcastStructure node) {
		val name_list = (#[node.in_variable.name] + node.out_list.map[it.name]).toList
		if (!name_list.contains(name.name)) {
			throw new RuntimeException("error")
		}
		// get type from flow connected to input
		var behavior = EcoreUtil2.getContainerOfType(name, SoflBehaviour)
		if (behavior === null) {
			throw new RuntimeException("error")
		}
		//get an output variable
		var filter = behavior.flow.filter [
			if (it.destination.variable_name !== null && it.destination.node.broadcast !== null) {
				(name_list.contains(it.destination.variable_name.name))
			} else {
				false
			}
		]
		if (filter.length == 0) {
			var sfilter = behavior.shadow_flow.filter [
				if (it.destination.connected_name !== null && it.destination.node.broadcast !== null) {
					name_list.contains(it.destination.connected_name.name == node.in_variable.name) 
				} else {
					false
				}
			]
			if (sfilter.length == 0) {
				new ErrorType('''type of «name.name» in «node.name» cannot be computed''')
			}
			var found = sfilter.head.source
			//getVariableType(found.connected_name, found.node)
			new ShaddowType(found.connected_name.name)		
		}
		var found = filter.head.source
		getVariableType(found.variable_name, found.node)
	}

	def static TypeBase getVariableType(SoflVariableName name, ConditionStructure node) {
		var type = NodeBaseType::create(node)
		type.in_types.head.head.type
	}

	def static TypeBase getVariableType(SoflVariableName name, NodeName node) {
		if (node.is_behavior) {
			var behavior = EcoreUtil2.getContainerOfType(name, SoflBehaviour)
			getVariableType(name, behavior)
		} else if (node.proc !== null) {
			getVariableType(name, node.proc)
		} else if (node.cond !== null) {
			getVariableType(name, node.cond)
		} else if (node.merge !== null) {
			getVariableType(name, node.merge)
		} else if (node.unmerge !== null) {
			getVariableType(name, node.unmerge)
		} else if (node.broadcast !== null) {
			getVariableType(name, node.broadcast)
		} else {
			throw new RuntimeException("error")
		}
	}

	def static TypeBase getVariableType(SoflVariableName name, MergeStructure node) {
		val merge_type = new MergeType(node)
		//input type
		val in_canidates = merge_type.in_types.head.filter[it.varaible == name.name]
		if (in_canidates.length > 1) {
			throw new RuntimeException("error")
		} else if (in_canidates.length == 1) {
			return in_canidates.head.type
		}
		//output type
		if(name.name == merge_type.out_types.head.head.varaible){
			return merge_type.out_types.head.head.type				
		}else{
			throw new RuntimeException("error")
		}
	}

	def static TypeBase getVariableType(SoflVariableName name, UnMergeStructure node) {
		val merge_type = new UnMergeType(node)
		//output type
		val in_canidates = merge_type.out_types.head.filter[it.varaible == name.name]
		if (in_canidates.length > 1) {
			throw new RuntimeException("error")
		} else if (in_canidates.length == 1) {
			return in_canidates.head.type
		}
		//input type
		if(name.name == merge_type.in_types.head.head.varaible){
			return merge_type.in_types.head.head.type				
		}else{
			throw new RuntimeException("error")
		}
	}

	def static TypeBase getVariableType(SoflVariableName name, SoflBehaviour behavior) {
		for (el : behavior.in_ports.ports) {
			if (el.declare !== null) {
				for (e : el.declare) {
					if (e.identifier.exists[it.name == name.name]) {
						return getType(e.type)
					}
				}
			}
		}
		for (el : behavior.out_ports.ports) {
			if (el.declare !== null) {
				for (e : el.declare) {
					if (e.identifier.exists[it.name == name.name]) {
						return getType(e.type)
					}
				}
			}
		}
		if (behavior?.adding?.in_added?.declare !== null) {
			for (el : behavior.adding.in_added.declare) {
				if (el.identifier.exists[it.name == name.name]) {
					return getType(el.type)
				}
			}
		}
		if (behavior?.adding?.out_added?.declare !== null) {
			for (el : behavior.adding.out_added.declare) {
				if (el.identifier.exists[it.name == name.name]) {
					return getType(el.type)
				}
			}
		}

		new ErrorType('getVariableInProcessType')
	}

	// type must be able to accept modifier, this function of map, of composite, or list
	def static getVariableType(SoflVariableName name, List<FunctionReferenceModifiers> list) {
		var current_type = getVariableType(name)
		for (mod : list) {
			current_type = apply_modifier(current_type, mod)
		}
		return current_type
	}

	def static getVariableType(SoflVariableName name, SoflProcess process) {
		for (el : process.in_ports.ports) {
			if (el.declare !== null) {
				for (e : el.declare) {
					if (e.identifier.exists[it.name == name.name]) {
						return getType(e.type)
					}
				}
			}
		}
		for (el : process.out_ports.ports) {
			if (el.declare !== null) {
				for (e : el.declare) {
					if (e.identifier.exists[it.name == name.name]) {
						return getType(e.type)
					}
				}
			}
		}
		new ErrorType('getVariableInProcessType')
	}

	def static TypeBase getVariableType(SoflVariableName name, SoflModule module) {
		var in_function = EcoreUtil2.getContainerOfType(name, SoflFunction)
		if (in_function !== null) {
			return getVaraibleType(name, in_function)
		}
		var var_new_store = EcoreUtil2.getContainerOfType(name, NewVariable)
		if (var_new_store !== null) {
			return getType(var_new_store.type)
		}
		var var_external = EcoreUtil2.getContainerOfType(name, ExternalVariable)
		if (var_external !== null) {
			if (var_external.var_name !== null) {
				var parent = var_external.var_name.eContainer
				if (parent instanceof NewVariable) {
				} else if (parent instanceof ExternalVariable) {
				}
				return new ErrorType('getVariableInModuleType 0')
			} else if (var_external.ext_var_name !== null) {
				return getType(var_external.type)
			}
		}
		// search in variable defined and access types
		if (module?.const_declare?.const_list !== null) {
			for (const : module.const_declare.const_list) {
				if (const.name.name == name.name) {
					var t_type = getType(const.expression)
					var t_value = Evaluate::evaluate(const.expression)
					ConstantStore::add(const.name.name, t_type, t_value)
					return t_type
				}
			}
		}
		if (module?.var_decl?.vars !== null) {
			for (dec : module.var_decl.vars) {
				if (dec.name.name == name.name) {
					return getType(dec.type)
				}
			}
		}
		if (module?.var_decl?.extVars !== null) {
			for (dec : module.var_decl.extVars) {
				if (name.name == dec.ext_var_name.name) {
					return getType(dec.type)
				} else if (name.name == dec.var_name.name) {
					return getVariableType(dec.var_name)
				}
			}
		}
		if(module.parent !== null){
			return getVariableType(name, module.parent)
		}
		new ErrorType('getVariableInModuleType 1')
	}

	def static getVaraibleType(SoflVariableName name, SoflFunction function) {
		// test input
		var inlst = function.in_ports.list.filter[it.list.map[c|c.name].contains(name.name)]
		if (inlst.length == 1) {
			return getType(inlst.head.type)
		} else if (inlst.length > 1) {
			throw new RuntimeException('error')
		}

		if (function.ret_var.name == name.name) {
			return getType(function.ret_type)
		}
		throw new RuntimeException('error')
	}

	def static SoflFunction getFunction(SoflVariableName name, SoflModule module) {
		for (func : module.func_spec) {
			if (func.name.name == name.name) {
				return func
			}
		}

		if (module.parent !== null) {
			return getFunction(name, module.parent)
		} else {
			return null
		}

	}

	def static TypeBase getVariableType(SoflVariableName name, ExpressionSequence parameters) {
		var object_type = getVariableType(name)
		if (object_type instanceof MapType) {
			if (parameters.exp_list.length > 1) {
				new ErrorType('''Too many parameters given to «name.name»''')
			} else {
				var parameter_type = getType(parameters.exp_list.head)
				if (parameter_type == object_type.get_domain_type) {
					return object_type.get_range_type
				} else {
					new ErrorType('''Parameters «parameter_type» does not match required by «name.name»''')
				}

			}
		} else if (object_type instanceof ProductType) {
			if (parameters.exp_list.length > 1) {
				new ErrorType('''Too many parameters given to «name.name»''')
			} else {
				var parameter_type = getType(parameters.exp_list.head)
				if (parameter_type instanceof Natural0Type || parameter_type instanceof NaturalType) {
					return object_type.typeSet
				} else {
					new ErrorType('''The type «parameter_type» not allowed as parameter for «name.name»''')
				}
			}
		} else {
			new ErrorType('''«name.name» is not of varaible type but of type «object_type»''')
		}
	}

	def static TypeBase getVariableType(SoflVariableName name, ExpressionSequence parameters, SoflFunction function) {
		if (name.name == function.ret_var.name) {
			return getType(function.ret_type)
		} else if (name.name == function.name.name) {
			val function_type = Type::getType(function) as FunctionType
			val parameter_type = parameters.exp_list.map[getType(it)]
			if (parameter_type == function_type.get_parameters()) {
				return function_type.get_return_type
			} else {
				var str = '''parameters does not match «function_type.get_parameters»'''
				return new ErrorType(str)
			}
		} else {
			for (el : function.in_ports.list) {
				if (el.list.exists[it.name == name.name]) {
					return getType(el.type)
				}
			}
		}
		var str = '''The variable is not defined in the function name «name.name» function «function.name.name» cmp «name.name == function.name.name»'''
		return new ErrorType(str)
	}

	protected def static TypeBase apply_modifier(TypeBase type, FunctionReferenceModifiers modifier) {
		// val type_type = TypeName.getType(type)
		if (modifier.arg !== null) {
			val arg_type = getType(modifier.arg.exp_list.head)
			if (type instanceof MapType) {
				if (modifier.arg.exp_list.length == 1) {
					if (type.get_domain_type == arg_type) {
						return type.get_range_type
					}
				} else {
					return new ErrorType('''apply more than oe parameter to «type»''')
				}
			} else if (type instanceof FunctionType) {
				var parameters = getTypeList(modifier.arg)
				if (parameters == type.get_parameters()) {
					return type.get_return_type()
				} else {
					return new ErrorType('''«parameters» does not match function parameters «type.get_parameters()»''')
				}

			} else if (type instanceof SeqType) {
				var parameters = getTypeList(modifier.arg)
				if (parameters.length == 1) {
					if (parameters.head.is_discrete_number) {
						return type.elementType
					}
				} else if (parameters.length == 2) {
					if (parameters.get(0).is_discrete_number && parameters.get(1).is_discrete_number) {
						return type
					}
				}
			}
		} else if (modifier.member !== null) {
			if (type instanceof ComposedType) {
				return type.get_type_of(modifier.member.name)
			}
			return new ErrorType('''«type» does not allow access of member «modifier.member.name»''')

		}
		return type
	}

	def static TypeBase getType(PreDefinedFunctions function) {

		if (function.is_override) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('override function need two arguments')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				if (!type_list.head.is_map_type) {
					return new ErrorType('first parameter must be of map type')
				}
				var all_same = type_list.forall[it == type_list.head]
				if (all_same) {
					return type_list.head
				} else {
					return new ErrorType('override input not the same type')
				}
			}
		} else if (function.is_modify) {
			var first_type = getType(function.first)
			if (first_type instanceof ProductType) {
				if (function.arg_mod !== null) {
					for (arg : function.arg_mod.list) {
						var dest_type = getType(arg.dest)
						if (arg.val_src instanceof ExpNumber) {
							val src_type = getType(arg.val_src)
							if (src_type instanceof Natural0Type || src_type instanceof NaturalType) {
								var value = Evaluate::evaluate((arg.val_src as ExpNumber).number)
								var int_value = value.intValue
								if (int_value <= 0 || int_value > first_type.getTypeCount) {
									return new ErrorType('''index «int_value» out of bound  «first_type»''')
								}
								var req_type = (first_type as ProductType).getTypeAt(int_value - 1)
								if (req_type == dest_type) {
									return first_type
								} else {
									return new ErrorType('''«req_type» is not the type at «value» of the product type «first_type»''')
								}
							} else {
								return new ErrorType('''index must be a positive number «src_type»''')
							}
						} else {
							return new ErrorType('override parameter ? -> ? type mismatch')
						}

					}
				} else {
					return new ErrorType('override parameter ? -> ? needed')
				}
			} else if (!first_type.is_composed_type()) {
				return new ErrorType('modify invalid first type')
			} else if (function.args !== null) {
				return new ErrorType('modify invalid type of second and after arguments')
			}
		} else if (function.is_mk) {
			if (function.args.exp_list.length == 0) {
				return new ErrorType('''mk_TYPE not enough parameters supplied «function.args.exp_list.length»''')
			}
			val target_type = getType(function.type)
			if (target_type instanceof ComposedType) {
				var type_list = function.args.exp_list.map[getType(it)]
				if (target_type.can_create_from(type_list)) {
					return target_type
				} else {
					return new ErrorType('''mk_TYPE targer type «target_type» received parameters «type_list»''')
				}
			} else if (target_type instanceof ProductType) {
				var type_list = function.args.exp_list.map[getType(it)]
				val tmp_ret = new ProductType(type_list)
				if (tmp_ret == target_type) {
					return target_type
				} else {
					return new ErrorType('mk_TYPE parameters does not match product type')
				}
			} else {
				return new ErrorType('mk_TYPE type must be of composed or product type')
			}
		} else if (function.is_bound) {
			return new BooleanType()
		} else if (function.is_card) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('card require only one parameter')
			} else {
				var type = getType(function.args.exp_list.head)
				if (!type.is_set) {
					return new ErrorType('card parameter must be of type set')
				}
				return new Natural0Type()
			}
		} else if (function.is_subset || function.is_psubset) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('subset require only two parameter')
			} else {
				var type_list = function.args.exp_list.map[getType(it)]
				if (!type_list.head.is_set) {
					return new ErrorType('subset parameter must be of type set')
				}
				if (type_list.get(0) == type_list.get(1)) {
					return new BooleanType()
				}
			}
		} else if (function.is_get) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('get require only one parameter')
			} else {
				val type = getType(function.args.exp_list.head)
				if (type.is_set) {
					return (type as SetType).domainType
				}
			}
		} else if (function.is_union || function.is_inter || function.is_diff) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('''require only two parameter got «function.args.exp_list.length»''')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				if (type_list.head.is_set) {
					if (type_list.get(0) == type_list.get(1)) {
						return type_list.head
					}
					return new ErrorType('parameters must be of the same type')
				} else {
					return new ErrorType('set type is require as input')
				}
			}
		} else if (function.is_dunion || function.is_dinter) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require only one parameter got «function.args.exp_list.length»''')
			} else {
				val type = getType(function.args.exp_list.head)
				if (type.is_set) {
					return type
				} else {
					return new ErrorType('''set type is require as input «type»''')
				}
			}
		} else if (function.is_power) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require only one parameter got «function.args.exp_list.length»''')
			} else {
				val type = getType(function.args.exp_list.head)
				if (type.is_set) {
					return new SetType(type)
				} else {
					return new ErrorType('set type is require as input')
				}
			}
		} else if (function.is_inverse) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require only one parameter got «function.args.exp_list.length»''')
			} else {
				val type = getType(function.args.exp_list.head)
				if (type instanceof MapType) {
					return new MapType(type.get_range_type, type.get_domain_type)
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_comp) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('''require two parameter got «function.args.exp_list.length»''')
			} else {
				var type_list = function.args.exp_list.map[getType(it)]
				var f = type_list.get(0)
				var s = type_list.get(1)
				if ((f instanceof MapType) && (s instanceof MapType)) {
					var ff = f as MapType
					var ss = s as MapType
					if (ff.get_range_type == ss.get_domain_type) {
						return new MapType(ff.get_domain_type, ss.get_range_type)
					} else {
						return new ErrorType('range of first map does not match domain of second map')
					}
				} else {
					return new ErrorType('parameters must be of type map')
				}
			}
		} else if (function.is_domrb || function.is_domrt) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('''require two parameter got «function.args.exp_list.length» parameters''')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				var f = type_list.get(0)
				var s = type_list.get(1)
				if (f instanceof SetType) {
					if (s instanceof MapType) {
						if (f.domainType == s.get_domain_type) {
							return s
						} else {
							return new ErrorType('''the first parameter does not match the domain of the map f:«f.domainType» domain:«s.get_domain_type»''')
						}
					} else {
						return new ErrorType('second parameter must be of map type')
					}
				} else {
					return new ErrorType('first parameter must be of set type')
				}
			}
		} else if (function.is_dom) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				var f = type_list.get(0)
				if (f instanceof MapType) {
					return f.get_domain_type
				} else {
					return new ErrorType('parameter must be of type map')
				}
			}
		} else if (function.is_rng) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				var f = type_list.get(0)
				if (f instanceof MapType) {
					return f.get_range_type
				} else {
					return new ErrorType('parameter must be of type map')
				}
			}
		} else if (function.is_rngrb || function.is_rngrt) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('''require two parameter got «function.args.exp_list.length»''')
			} else {
				val type_list = function.args.exp_list.map[getType(it)]
				var f = type_list.get(0)
				if (f instanceof MapType) {
					var s = type_list.get(1)
					if (s instanceof SetType) {
						if (f.get_range_type == s.domainType) {
							return f
						} else {
							return new ErrorType('the second parameter does not match the range of the map')
						}
					} else {
						return new ErrorType('second parameter must be of set type')
					}
				} else {
					return new ErrorType('first parameter must be of map type')
				}
			}
		} else if (function.is_elems) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require only one parameter got «function.args.exp_list.length»''')
			} else {
				val type = getType(function.args.exp_list.head)
				if (type instanceof SeqType) {
					return new SetType(new NaturalType())
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_cons) {
			if (function.args.exp_list.length != 2) {
				return new ErrorType('''require two parameter got «function.args.exp_list.length»''')
			} else {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				val s = type.get(1)
				if (f instanceof SeqType) {
					if (s instanceof SeqType) {
						if (f == s) {
							return s
						}
					}
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_dcons) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				if (f instanceof SeqType) {
					if (f.elementType.is_sequnce) {
						return f.elementType
					} else {
						return new ErrorType('seq must be of sequences')
					}
				} else {
					return new ErrorType('seq type is require as input')
				}
			}
		} else if (function.is_len) {
			if (function.args.exp_list.length == 1) {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				if (f instanceof SeqType) {
					return new Natural0Type()
				} else {
					return new ErrorType('map type is require as input')
				}
			} else if (function.args.exp_list.length == 2) {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				val s = type.get(1)
				if (f.is_discrete_number) {
					if (s.is_discrete_number) {
						return new Natural0Type()
					}
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_hd) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				if (f instanceof SeqType) {
					return f.elementType
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_tl) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				if (f instanceof SeqType) {
					return f
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		} else if (function.is_inds) {
			if (function.args.exp_list.length != 1) {
				return new ErrorType('''require one parameter got «function.args.exp_list.length»''')
			} else {
				val type = function.args.exp_list.map[getType(it)]
				val f = type.get(0)
				if (f instanceof SeqType) {
					return new SetType(new NaturalType)
				} else {
					return new ErrorType('map type is require as input')
				}
			}
		}

		return new ErrorType('PreDefinedFunctions')
	}

	def static TypeBase getType(Expression first, ArgumentsModify name) {
		if (name !== null) {
			val t_first = getType(first)
			return t_first
		}
		return new ErrorType('Invalid ArgumentsModify')
	}

	def static getType(SoflFunction name, ExpressionSequence args) {
		return new ErrorType('getType(SoflFunction name, Arguments args) {')
	}

	def static getType(ComposedElement member) {
		return getType(member.type)
	}

	def static getType(SoflNumber number) {
		if (number.fraction !== null || number.is_real) {
			return new RealType()
		} else if (number.is_int) {
			return new IntegerType()
		} else if (number.is_nat0) {
			return new Natural0Type()
		} else if (number.is_nat) {
			return new NaturalType()
		}
		return new RealType
	}

	def static TypeBase getType(CaseExpression case_exp) {
		var cases = new ArrayList<TypeBase>
		if (case_exp.expression !== null) {
			cases.add(getType(case_exp.expression))
		}
		for (case_le : case_exp.case_item) {
			cases.add(getType(case_le.result))
		}
		val first = cases.head

		var all_the_same = cases.forall[it|it == first]
		if (all_the_same) {
			return first
		} else {
		}
		return new ErrorType('getType(CaseExpression case_exp)')
	}

	def static getType(EnumerateValue exp) {
		var type = EcoreUtil2.getContainerOfType(exp, SoflType)
		return getType(type)
	}

	def static TypeBase getType(TypeIdentifier exp) {
		// val v = EcoreUtil2.getContainerOfType(exp, TypeDecl)
		return getType((exp.eContainer as NewType).type)
	}

	def static isRefinedBy(ProcessType process, BehaviorType behavior) {
		if (process.in_list.length < behavior.in_list.length) {
			return false
		} else if (process.out_list.length < behavior.out_list.length) {
			return false
		}
		var behavior_index = 0
		var process_index = 0

		// check in the in types
		while (process_index < process.in_list.length) {
			var currrent_type = process.in_list.get(process_index)
			var not_exit = false
			var previouse = behavior_index
			while (!not_exit && behavior_index < behavior.in_list.length) {
				if (currrent_type.isPortExtendedBy(behavior.in_list.get(behavior_index))) {
					behavior_index = behavior_index + 1
				} else {
					if (previouse == behavior_index) {
						return false
					}
					not_exit = true
				}
			}
			process_index = process_index + 1
		}

		if (process_index != process.in_list.length && behavior_index != behavior.in_list.length) {
			return false
		}

		behavior_index = 0
		process_index = 0

		// check the out types
		while (process_index < process.out_list.length) {
			var currrent_type = process.out_list.get(process_index)
			var not_exit = false
			var previouse = behavior_index
			while (!not_exit && behavior_index < behavior.out_list.length) {
				if (currrent_type.isPortExtendedBy(behavior.out_list.get(behavior_index))) {
					behavior_index = behavior_index + 1
				} else {
					if (previouse == behavior_index) {
						return false
					}
					not_exit = true
				}
			}
			process_index = process_index + 1
		}

		if (process_index != process.out_list.length && behavior_index != behavior.out_list.length) {
			return false
		}
		return true
	}

	def static isBoolean(Expression exp) {
		getType(exp).is_boolean()
	}

	def static getTypeList(ExpressionSequence arguments) {
		arguments.exp_list.map[getType(it)]
	}
}
