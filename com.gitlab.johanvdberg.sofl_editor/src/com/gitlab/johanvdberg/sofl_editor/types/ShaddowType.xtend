package com.gitlab.johanvdberg.sofl_editor.types

class ShaddowType extends VariableType {			

	new(String type) {
		super(type, new ConnectionType)
	}
	
	override toString(){
		'''(«this.varaible» «this.varaible»)'''
	}
}