package com.gitlab.johanvdberg.sofl_editor.types


import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure

class ConditionType extends NodeBaseType {
	
	protected new(ConditionStructure node) {
		super(TypeBase::CONDITION_TYPE)
		val v_type = getType(node, true, node.in_variable)
		this.in_types.add(newHashSet(#[new VariableType(node.in_variable.name, v_type)]))
		this.out_types.add(newHashSet(node.out_list.map[current|new VariableType(current.varname.name, v_type)]))
	}
	
}
