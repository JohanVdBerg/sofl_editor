package com.gitlab.johanvdberg.sofl_editor.syntax

import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName

class Names {
	
	def static String getName(ConnectionName name) {
		'''connection_«name.name»'''
	}
	
}
