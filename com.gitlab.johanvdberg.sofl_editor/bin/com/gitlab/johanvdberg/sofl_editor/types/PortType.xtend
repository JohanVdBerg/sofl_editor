package com.gitlab.johanvdberg.sofl_editor.types

import com.google.common.collect.Multiset
import com.google.common.collect.TreeMultiset
import java.util.List

class PortType {
	public Multiset<TypeBase> types
	
	new(){
		this.types = TreeMultiset.create()
	}
	
	def add(TypeBase type){
		this.types.add(type)
	}
	
	def addAll(List<TypeBase> type_list){
		for(type:type_list){
			this.add(type)		
		}
	}
	
	def isPortExtendedBy(PortType other){
		var other_types = other.types.elementSet
		for(current: this.types.elementSet){
			if(other_types.contains(current)){
				if(this.types.count(current) <= other.types.count(current)){
					return true
				}else{
					return false
				}
			}else{
				return false
			}
		}
	}
	
	override toString(){
		var ret = ""
		for(element: this.types.elementSet){
			ret = ''' «element» («this.types.count(element)»)'''
		}
		ret
	}
	
	
}
