
package com.gitlab.johanvdberg.sofl_editor.types

class SetType extends TypeBase{
	
	TypeBase element_type
	new(TypeBase set_elements_type){
		super(TypeBase::SET_TYPE)
		this.element_type = set_elements_type
	}
	
	def getDomainType(){
		return this.element_type
	}
		
	def getElementType(){
		return this.element_type
	}
	
	override toString(){
		'''set(«this.element_type»)'''
	}
	
	override equals(Object o){
		if(o instanceof SetType){
			return this.typeClass == o.typeClass
		}
		return super.equals(o)
	}	
}
