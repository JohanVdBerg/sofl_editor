package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.syntax.Names

class BroadcastType extends NodeBaseType{
	
	new(BroadcastStructure node) {
		super(TypeBase::BROADCAST_TYPE)
		if(node.in_variable !== null){
			this.in_types.add(newHashSet(#[new VariableType(node.in_variable.name, NodeBaseType::getType(node, true, node.in_variable))]))
			this.out_types.add(newHashSet(node.out_list.map[current|new VariableType(current.name, NodeBaseType::getType(node, false, current))]))
		}else{
			this.in_types.add(newHashSet(#[new VariableType(Names::getName(node.in_connect), new ConnectionType)]))
			this.out_types.add(newHashSet(node.out_connect.map[current|new VariableType(Names::getName(current), new ConnectionType)]))
		}
	}
	
}
