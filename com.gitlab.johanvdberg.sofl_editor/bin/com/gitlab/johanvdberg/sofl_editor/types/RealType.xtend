package com.gitlab.johanvdberg.sofl_editor.types

class RealType extends NumericType{
	
	new(){
		super(TypeBase::REAL_TYPE)
	}
	
}
