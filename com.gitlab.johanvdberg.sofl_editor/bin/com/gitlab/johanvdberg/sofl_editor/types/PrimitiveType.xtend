package com.gitlab.johanvdberg.sofl_editor.types

abstract class PrimitiveType extends TypeBase{
	
	new(String type){
		super(type)
	}
	
	override equals(Object o){
		if(o instanceof PrimitiveType){
			return this.typeClass == o.typeClass
		}
		return super.equals(o)
	}
	
}
