package com.gitlab.johanvdberg.sofl_editor.types

class ErrorType extends TypeBase{	
	String str
	new( String str){
		super('Error Type')
		this.str = str
		if(str != ''){
			//throw new RuntimeException(str)			
		}
	}
	
	override is_error(){
		return true
	}
	
	override toString(){
		"Error: " + this.str
	}
}
