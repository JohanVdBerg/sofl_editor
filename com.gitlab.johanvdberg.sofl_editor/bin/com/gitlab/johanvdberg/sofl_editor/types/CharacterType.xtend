package com.gitlab.johanvdberg.sofl_editor.types

class CharacterType extends PrimitiveType{
	
	new(){
		super(TypeBase::CHAR_TYPE)
	}
	
}