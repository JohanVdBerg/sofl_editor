package com.gitlab.johanvdberg.sofl_editor.types

class SeqType extends TypeBase{
	TypeBase element_type
	new(TypeBase seq_element_type){
		super(TypeBase::SEQ_TYPE)
		element_type = seq_element_type
	}
	
	def getElementType(){
		return this.element_type
	}

	override equals(Object o){
		if(o instanceof SeqType){
			return this.element_type == o.element_type
		}else{
			return super.equals(o)		
		}
	}
	override toString(){
		return '''seq<«this.element_type»>'''
	}
}
