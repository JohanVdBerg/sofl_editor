package com.gitlab.johanvdberg.sofl_editor.generator.smt

import java.util.HashMap
import java.util.Map
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligation
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationFlowPredicate
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementUp
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementDown
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationSatisfiable
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationImply
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.smt.SmtModule
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationPredicateTransform
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFile

class SmtProveModule {
	String name
	public Map<String, SmtProveNode> nodes
	public SmtProveBahviour behaviour
	ProofObligationSatisfiable proof_obligation
	Map<String, SmtFile> proof_inv_file
	public Map<String, SmtProveControl> control_nodes
	SmtModule module
	String base_location
	IFileSystemAccess2 fsa
	
	new(SmtModule module, String smt_location, IFileSystemAccess2 fsa) {
		if(module.behavior === null){
			throw new RuntimeException("Behaviour need to be defined")
		}
		this.module = module
		this.base_location = smt_location
		this.fsa = fsa
		this.name = module.name
		this.nodes = new HashMap<String, SmtProveNode>
		this.control_nodes = new HashMap<String, SmtProveControl>
		
		this.behaviour = new SmtProveBahviour(module.behavior, base_location)
		this.control_nodes.putAll(this.behaviour.controlNodesProves)
		
		var inv_list = new ArrayList<SmtCondition>
		inv_list.addAll(module.initInvariants)
		inv_list.addAll(module.invariants)
		inv_list.add(SmtCondition::imply(SmtCondition::conjunction(module.invariants), SmtCondition::conjunction(module.initInvariants)))
		
		this.proof_obligation = new ProofObligationSatisfiable("invariant consistent", inv_list)
		this.proof_inv_file = new HashMap<String, SmtFile>
		var file_name = '''«base_location»/invariant_«this.name».sat'''
		this.proof_inv_file.put(
			file_name,
			new SmtFile(
				base_location, '''invariant_«this.name».sat''',
				SmtFile::ProofType::Constrain,
				this.proof_obligation.smt_proof
			)
		)
		//for processes nodes
		for(node: module.allProcesses){
			nodes.put(node.name, new SmtProveNode(node, this.base_location))
		}
		//for control nodes - condition node
		
	}	
	
	def getName(){
		this.name
	}
	
	def write_smt(){
		for(now: this.proof_inv_file.keySet){
			this.proof_inv_file.get(now).write_file(fsa)
		}
		//controls		
		for(now: this.nodes.values){
			now.write_file(fsa)
		}
		for(now: this.control_nodes.values){
			now.write_file(fsa)
		}
		this.behaviour.write_file(fsa)
	}
	
	def write_script(){
		var content = 
		'''
		«this.behaviour.get_smt_text»
		
		
		echo "Test if the condition node are valid"
		
		«FOR node_name: this.nodes.keySet»
			echo "Solve smt for node «node_name»"
			«FOR port_id: this.nodes.get(node_name).proof_start.keySet»
				echo "Proof constrain on inport «port_id»: «this.nodes.get(node_name).proof_start.get(port_id).fileName»"
				z3 -smt2 "«this.nodes.get(node_name).proof_start.get(port_id).fileName»"
			«ENDFOR»
			«FOR port_id: this.nodes.get(node_name).proof_end.keySet»
				echo "Proof constrain on outport «port_id»: «this.nodes.get(node_name).proof_end.get(port_id).fileName»"
				z3 -smt2 "«this.nodes.get(node_name).proof_end.get(port_id).fileName»"
			«ENDFOR»
		«ENDFOR»
		'''
		fsa.generateFile('''«base_location»/prove_smt.sh''', content)
	}
	
	def write_markdown() {
		var content = '''
		
		Proof by SMT-solver
		===================
		
				
		Module SMT: «name»
		«FOR i:0..<name.length + 11»=«ENDFOR»
		
		
		«IF module.have_parent»
		The parent of this module add constrains that need to be verified when validating the behavior (CDFD). This module must refine a 
		process «module.get_refined_process» in the parent module «module.get_refined_process»
		
		«ELSE»
		This is a top module. The invariant of the module is used to create the execution environment and the current port of the CDFD 
		that starts executon of the diagram. 
		«ENDIF»
		
		
		
		The invariant equation of the module is given by
		
		
		|        Invariant      |
		|-----------------------|
		«IF this.module.invariants.length > 0»	
		«FOR exp:this.module.invariants»		
		| «exp.get_formula.smt.replace("\n"," ")» |
		«ENDFOR»
		«ELSE»
		|  None                 |
		«ENDIF»
		
		
		| Init Process Invariant  |
		|-------------------------|
		«IF this.module.initInvariants.length > 0»	
		«FOR exp:this.module.initInvariants»		
		| «exp.get_formula.smt.replace("\n"," ")» |
		«ENDFOR»
		«ELSE»
		|  None                   |
		«ENDIF»
				
		The proce script to to show that the invariant and inital conditions are satifiable
		
		«this.proof_inv_file.values.head.write_markdown»
		
		«behaviour.write_markdown»
		
		«FOR node:this.nodes.values SEPARATOR '\n'»
			«node.write_markdown»
		«ENDFOR»
		
		«FOR node:this.control_nodes.values SEPARATOR '\n'»
			«node.write_markdown»
		«ENDFOR»
		
		'''
		
		fsa.generateFile('''«base_location»/description.md''', content)
	}
	

	
	
	static def mark_down(ProofObligation condition){
		if (condition instanceof ProofObligationPredicateTransform){
			mark_down(condition)
		}else if (condition instanceof ProofObligationRefinementDown){
			mark_down(condition)
		}else if (condition instanceof ProofObligationRefinementUp){
			mark_down(condition)
		}else if(condition instanceof ProofObligationSatisfiable){
			mark_down(condition)
		}else if(condition instanceof ProofObligationFlowPredicate){	
			mark_down(condition)
		}else{
			throw new RuntimeException('add mark down write')
		}
	}
	static def mark_down(ProofObligationFlowPredicate obligation){
		'''
		A number of data flows are connected to a port. It is nessacery to show that pre-condition is satisfiable
		n the current context
		
		- the environment and invariant must be satisfiable
		- the environment and invariant must imply the oblication
		
		| Environment define by flow source |
		|-----------------------------------|
		«FOR env: obligation.environment»
		|«env.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		| Invariant of the module |
		|-------------------------|
		«FOR inv: obligation.invariants»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		The obligation statment is given by 
		 
		    «obligation.obligation»
		
		The condition that need to be proof is given by `forall s in S. environment and invariant imply condition` where `S` consist of the free variable in `environment` and `invariant`
		'''
	}
	
	static def mark_down(ProofObligationPredicateTransform obligation){
		'''
		Here it is neededd that a pre condition will result in an ouput condition in the current environement
		
		The allowed end states of a predicate transform need to be verified by making sure the conjunction of the following is satifiable:
		
		- The invariant
		- The environment where all variable are renamed to variables as before the state transition
		- the oblication
		
		| Start invariant |
		|-----------------|
		«FOR current: obligation.start_invariants»
		|«current.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		| Precondition |
		|--------------|
		|«obligation.pre_condition.get_formula.smt.replace("\n"," ")»|
		
		
		
		| End invariant |
		|---------------|
		«FOR current: obligation.end_invariants»
		|«current.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		| Postcondition |
		|---------------|
		|«obligation.post_condition.get_formula.smt.replace("\n"," ")»|

		'''
	}
	
	static def mark_down(ProofObligationImply obligation){
		'''
		Condition `P`: 
		
		    «obligation.P»
		
		Condition `Q`: 
		
			«obligation.Q»
			
		The variable over which the quantifier is defined
		
		| Variables |
		|-----------|
		«FOR v : obligation.imply_scope.pairs»
		|«v.toString»|
		«ENDFOR»
		
		The implication statement:
		
		    «obligation.imply_condition»
		'''
		
	}
	
	static def mark_down(ProofObligationRefinementDown obligation){
		'''
		The refinement relation from the refining process to this module is verified by:
		
		- The upper invariant and the conditions of the associated ports (environment) in the refinemend process must be valid
		- The upper invariant and environment imply the refined ports condition (obligation) and the lower invariant
		
		| Refined port conditions |
		|------------------------|
		«FOR env: obligation.refined_port_conditions»
		| «env.get_formula.smt.replace("\n"," ")»  |
		«ENDFOR»

		| Upper Invariant  |
		|------------------|
		«FOR inv: obligation.upper_invariants»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		
		|   Lower Invariant   |
		|---------------------|
		«FOR inv: obligation.lower_invariants»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		

		
		The condition of the CDFD port is given by 
		    
		    «obligation.obligation.toString.replace("\n"," ").replace("  "," ")»
		
		
		«mark_down(obligation as ProofObligationImply)»
		
		The condition that need to be proof is given by `forall s in S. environment and upper_invariant imply 
		condition and lower_invariant` where `S` consist of the free variable in `environment` and `upper_invariant`
		'''
	}
	
	static def mark_down(ProofObligationRefinementUp obligation){
		'''
		
		|  Lower invaraint |
		|------------------|
		«FOR inv: obligation.lower_invariants»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		|   Lower Port condition  |
		|-------------------------|
		|«obligation.lower_obligation.toString.replace("\n"," ")»  |
				
		
		|  Upper invaraint |
		|------------------|
		«FOR inv: obligation.upper_invariants»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		|   Upper Port conditions |
		|-------------------------|
		«FOR inv: obligation.upper_obligation»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		
		«mark_down(obligation as ProofObligationImply)»
		'''
	}
		
	static def mark_down(ProofObligationSatisfiable obligation){
		'''
		The predicates must be satiafiable
		
		|    Predicates    |
		|------------------|
		«FOR inv: obligation.condition»
		|«inv.get_formula.smt.replace("\n"," ")»|
		«ENDFOR»
		'''
	}
	
}
