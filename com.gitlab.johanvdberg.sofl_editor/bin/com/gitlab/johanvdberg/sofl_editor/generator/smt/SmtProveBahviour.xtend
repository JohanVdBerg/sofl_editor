package com.gitlab.johanvdberg.sofl_editor.generator.smt

import com.gitlab.johanvdberg.sofl_editor.smt.SmtBehviorNode
import java.util.HashMap
import java.util.Map
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFile
import com.gitlab.johanvdberg.sofl_editor.smt.SmtTransitionPorts

class SmtProveBahviour {
	
	SmtBehviorNode behaviour
	Map<String, SmtFile> start_obligation
	Map<String, SmtFile> end_obligation
	Map<SmtTransitionPorts, SmtFile> transition_obligation
	Map<String, SmtFile> outflow__obligation
	String file_prefix
	
	new(SmtBehviorNode behaviour, String file_prefix){
		this.behaviour = behaviour
		this.file_prefix = file_prefix
		this.start_obligation = new HashMap<String, SmtFile>
		this.end_obligation = new HashMap<String, SmtFile>
		this.transition_obligation = new HashMap<SmtTransitionPorts, SmtFile>
		this.outflow__obligation = new HashMap<String, SmtFile>
		for(current: behaviour.refinement_down.keySet){
			this.start_obligation.put(current, 
				new SmtFile(file_prefix, '''start_condition_«behaviour.name»_inport_«current».smt''', 
					SmtFile.ProofType::StartState, 
					behaviour.refinement_down.get(current).smt_proof
				)
			) 
		}
		
		for(current: behaviour.refinement_up.keySet){
			this.end_obligation.put(current, 
				new SmtFile(file_prefix, '''start_condition_«behaviour.name»_outport_«current».smt''', 
					SmtFile.ProofType::EndState, 
					behaviour.refinement_up.get(current).smt_proof
				)
			)
		}
		
		for(port_id: behaviour.transition_obligations.keySet){
			this.transition_obligation.put(port_id, 
				new SmtFile(file_prefix, '''behaviour_transition_«behaviour.get_name»_outport_«port_id.toString.replace(' ','_')».smt2''',
					SmtFile::ProofType::EndState, 
					behaviour.transition_obligations.get(port_id).smt_proof
				)
			)
		}
		
		for(port_id: behaviour.flow_port_obligations.keySet){
			this.outflow__obligation.put(port_id, 
				new SmtFile(file_prefix, '''behaviour_«behaviour.get_name»_out__port_«port_id.toString.replace(' ','_')».smt2''',
					SmtFile::ProofType::EndState, 
					behaviour.flow_port_obligations.get(port_id).smt_proof
				)
			)
		}
	}
	
	def getControlNodesProves(){
		var lst = new HashMap<String, SmtProveControl>
		for(c_node: this.behaviour.condition_nodes){
			lst.put(c_node.name, new SmtProveControl(c_node, this.file_prefix))
		}
		return lst
	}
	
	def write_markdown(){
		'''
		
		
		«IF behaviour.have_parent»
		Refinement prove obligations
		----------------------------
		
		The proof obligations are as follow:
				
		### Input ports
				
		The associated input ports of the process begin refined are the source of the data and it is nessacery to show that the current port's cndition will evalaute to true
		«FOR in_obj: behaviour.refinement_down.keySet»
		#### Port «in_obj»
						
		«SmtProveModule::mark_down(behaviour.refinement_down.get(in_obj))»
		«this.start_obligation.get(in_obj).write_markdown»
		«ENDFOR»
						
						
		### Output ports
		«FOR in_obj: behaviour.refinement_up.keySet»
		#### Port «in_obj»
						
		«SmtProveModule::mark_down(behaviour.refinement_up.get(in_obj))»
		«this.end_obligation.get(in_obj).write_markdown»
		«ENDFOR»
		«ENDIF»
		
		## The transition obligations
		«FOR name: this.transition_obligation.keySet»
		### «name»
		«SmtProveModule::mark_down(this.behaviour.transition_obligations.get(name))»
		«this.transition_obligation.get(name).write_markdown»
		«ENDFOR»
		
		## Ouput flow obligations
		«FOR name: this.outflow__obligation.keySet»
		### «name»
		«SmtProveModule::mark_down(this.behaviour.flow_port_obligations.get(name))»
		«this.outflow__obligation.get(name).write_markdown»
		«ENDFOR»
		
		
		'''
	}
	
	def get_smt_text(){
		'''
		echo "Solve SMT for Module «this.behaviour.name»"
		echo "Run the initial state obligations proof file"
		«FOR port_id: this.start_obligation.keySet»
			echo "Solve for in port «port_id»: «this.start_obligation.get(port_id).fileName»"
			z3 -smt2 "«this.start_obligation.get(port_id).fileName»"
		«ENDFOR»
		echo "Run the end state obligations proof file"
		«FOR port_id: this.end_obligation.keySet»
			echo "Solve for out port «port_id»: «this.end_obligation.get(port_id).fileName»"
			z3 -smt2 "«this.end_obligation.get(port_id).fileName»"
		«ENDFOR»
		'''
	}
	
	def write_file(IFileSystemAccess2 access2) {
		for(key: this.start_obligation.keySet){
			this.start_obligation.get(key).write_file(access2)
		}
		for(key: this.end_obligation.keySet){
			this.end_obligation.get(key).write_file(access2)
		}
	}	
}
