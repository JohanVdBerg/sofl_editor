package com.gitlab.johanvdberg.sofl_editor.generator

import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import com.gitlab.johanvdberg.sofl_editor.semantics.Module
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule

class AlgebraGeneration {
	
	Module semantic_module
	ProcAlgGenerate proc_generation
	//InOutRelations in_out_relations
	ConsistencyGenerate consisteny_formulas
	SmtGenerator smt_generator
	
	new (SoflModule module, IFileSystemAccess2 fsa){
		this.semantic_module = new Module(module)
		var base_location = '''./«this.get_base_location(module)»'''
		this.proc_generation = new ProcAlgGenerate(this.semantic_module, base_location, fsa)
		this.consisteny_formulas = new ConsistencyGenerate(this.semantic_module, 
			base_location, fsa, this.proc_generation.file_name
		)
		this.smt_generator = new SmtGenerator(module, base_location, fsa)
	}
	
	
	def write(){
		//this.createEmf()
		this.proc_generation.write()
		//this.in_out_relations.generate()
		this.consisteny_formulas.write()
		this.smt_generator.write()
	}
	
	protected def String get_base_location(SoflModule module){
		return (if(module.parent!==null){this.get_base_location(module.parent) + '/'}else{'./'}) + module.name + '/'
	}
	
	def createEmf(){
		 EcorePackage.eINSTANCE.eClass();
    /*	Initialize your EPackage*/
    	val reg = Resource.Factory.Registry.INSTANCE
    	var m = reg.getExtensionToFactoryMap()
    	m.put(EcorePackage.eNAME, new XMIResourceFactoryImpl())

   		val resSet = new ResourceSetImpl();
    	
    	var resource = try {
        	var resource = resSet.getResource(URI.createFileURI("diagram.ns"), true);
        	resource
    	} catch (Exception e) {
        	e.printStackTrace();
        	throw e
    	}
    	resource
	}
}
