/*
 * generated by Xtext 2.14.0
 */
package com.gitlab.johanvdberg.sofl_editor.scoping

import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.CondOption
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Decomposition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.EnumerateValue
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEnum
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpScopeModify
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExternalVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.FlowDestination
import com.gitlab.johanvdberg.sofl_editor.soflDsl.FlowSource
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NodeName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PortReference
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PostCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PreCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ProcessVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ShaddowFlowDestinationNode
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ShaddowFlowSourceNode
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflDslPackage
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflFunction
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflInitProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Statecondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.TypeIdentifier
import com.gitlab.johanvdberg.sofl_editor.soflDsl.TypeLower
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName
import java.util.ArrayList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class SoflDslScopeProvider extends AbstractSoflDslScopeProvider {

	override getScope(EObject context, EReference reference) {
		// System.out.println("getScope")
		val module = EcoreUtil2.getContainerOfType(context.eContainer, SoflModule)
		var scope = IScope.NULLSCOPE
		var type_scope = getModuleTypeScope(module, scope)
		scope = if (reference == SoflDslPackage.Literals.SOFL_MODULE__PARENT) {
			super.getScope(context, reference)
		} else if (context instanceof NodeName) {
			scope = classScopeFor(module.proc_spec, scope)
			if (module.cdfd !== null) {
				if (module.cdfd.conditions !== null) {
					scope = classScopeFor(module.cdfd.conditions, scope)
				}
				if (module.cdfd.broadcasts !== null) {
					scope = classScopeFor(module.cdfd.broadcasts, scope)
				}
				if (module.cdfd.merge !== null) {
					scope = classScopeFor(module.cdfd.merge, scope)
				}
				if (module.cdfd.ummerge !== null) {
					scope = classScopeFor(module.cdfd.ummerge, scope)
				}
			}
		} else if (context instanceof Decomposition && reference == SoflDslPackage.Literals.DECOMPOSITION__MODULE) {
			// var tmp = super.getScope(context, reference).allElements.map[it.EObjectOrProxy].toList
			// var tmp1 = tmp.filter[it instanceof SoflModule].map[it as SoflModule].toList
			// var tmp2 = tmp1.filter[it.name !== module.name].toList
			// classScopeFor(tmp2)
			super.getScope(context, reference)
		} else if (context instanceof PortReference) {
			var behaviour = EcoreUtil2.getContainerOfType(context, SoflBehaviour)
			if (behaviour !== null) {
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.adding.in_added, SoflVariableName),
					scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.in_ports, SoflVariableName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.in_ports, ConnectionName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.adding.in_added, ConnectionName), scope)
				if (behaviour?.adding?.out_added !== null) {
					scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.adding.out_added, SoflVariableName),
						scope)
				}
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.out_ports, SoflVariableName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.out_ports, ConnectionName), scope)
				if (behaviour?.adding?.out_added !== null) {
					scope = classScopeFor(EcoreUtil2.getAllContentsOfType(behaviour.adding.out_added, ConnectionName),
						scope)
				}
				scope
			} else {
				var proces = EcoreUtil2.getContainerOfType(context, SoflProcess)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(proces.in_ports, SoflVariableName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(proces.in_ports, ConnectionName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(proces.out_ports, SoflVariableName), scope)
				scope = classScopeFor(EcoreUtil2.getAllContentsOfType(proces.out_ports, ConnectionName), scope)
			}
		} else if (context instanceof ExpEnum) {
			var tmp_scope = getModuleTypeScope(module, scope)
			var tt = tmp_scope.allElements.filter[it|it.getEObjectOrProxy instanceof EnumerateValue].map [ it |
				it.EObjectOrProxy
			]
			classScopeFor(tt.toList)
		} else if (context instanceof ExpScopeModify) {
			getExpressionTermScope(context, type_scope)
		} else if (context instanceof ExternalVariable && module.parent !== null) {
			var lst = EcoreUtil2.getAllContentsOfType(module.parent.var_decl, SoflVariableName)
			scope = classScopeFor(lst, scope)
			if (module?.parent?.const_declare?.const_list !== null) {
				var lst1 = module.parent.const_declare.const_list.map[it.name]
				scope = classScopeFor(lst1, scope)
			}
			scope

		} else if (context instanceof ProcessVariable) {
			var lst = EcoreUtil2.getAllContentsOfType(module.var_decl, SoflVariableName)
			classScopeFor(lst, scope)
		} else if (context instanceof TypeLower && reference == SoflDslPackage.Literals.TYPE_LOWER__TYPE_ID) {
			scope = getModuleTypeScope(module, IScope.NULLSCOPE)
			scope
		} else if (context instanceof FlowSource && reference == SoflDslPackage.Literals.FLOW_SOURCE__VARIABLE_NAME) {
			var t = context as FlowSource
			getFlow(t.node, true)
		} else if (context instanceof FlowDestination &&
			reference == SoflDslPackage.Literals.FLOW_DESTINATION__VARIABLE_NAME) {
			var t = context as FlowDestination
			getFlow(t.node, false)
		} else if (context instanceof ShaddowFlowSourceNode &&
			reference == SoflDslPackage.Literals.SHADDOW_FLOW_SOURCE_NODE__CONNECTED_NAME) {
			var t = context as ShaddowFlowSourceNode
			getFlow(t.node, true)
		} else if (context instanceof ShaddowFlowDestinationNode &&
			reference == SoflDslPackage.Literals.SHADDOW_FLOW_DESTINATION_NODE__CONNECTED_NAME) {
			var t = context as ShaddowFlowDestinationNode
			getFlow(t.node, false)
		} else if (context instanceof ShaddowFlowSourceNode) {
			getShadowSourceScope(context)
		} else if (context instanceof ShaddowFlowDestinationNode) {
			getShadowDestinationScope(context)
		} else {
			scope
		}
		if (reference != SoflDslPackage.Literals.SOFL_MODULE__PARENT) {
			return getFunctionInScope(module, scope)
		} else {
			return scope
		}
	}

	def IScope classScopeFor(Iterable<? extends EObject> name) {
		var names = name.filter[it !== null]
		if (names === null) {
			throw new RuntimeException('parameters names is null')
		} else {
			for (current : names) {
				if (current === null) {
					throw new RuntimeException('element in the list names is null')
				}
			}
		}
		Scopes.scopeFor(names)
	}

	def IScope classScopeFor(Iterable<? extends EObject> name, IScope scope) {
		var names = name.filter[it !== null]
		if (names === null) {
			throw new RuntimeException('parameters names is null')
		} else if (scope === null) {
			throw new RuntimeException('parameters scope is null')
		} else {
			for (current : names) {
				if (current === null) {
					throw new RuntimeException('element in the list names is null')
				}
			}
		}
		Scopes.scopeFor(names, scope)
	}

	def IScope getFunctionInScope(SoflModule module, IScope scope) {
		var tmp_scope = if (module.parent !== null) {
				getFunctionInScope(module.parent, scope)
			} else {
				scope
			}
		tmp_scope = if (module.func_spec !== null) {
			var lst = module.func_spec.map[it.name]
			classScopeFor(lst, tmp_scope)
		} else {
			tmp_scope
		}
		tmp_scope
	}

	def IScope getModuleTypeScope(SoflModule module, IScope scope) {
		if (module !== null) {
			var tmp_scope = if (module.parent !== null) {
					getModuleTypeScope(module.parent, scope)
				} else {
					scope
				}
			tmp_scope = if (module.types !== null) {
				var lst = EcoreUtil2.getAllContentsOfType(module, TypeIdentifier)
				classScopeFor(lst, tmp_scope)
			} else {
				tmp_scope
			}
			tmp_scope
		} else {
			scope
		}
	}

	def getShadowSourceScope(ShaddowFlowSourceNode node) {

		var scope_list = new ArrayList<ConnectionName>
		if (node?.node?.proc !== null) {
			if (node?.node?.proc?.out_ports?.ports === null) {
				throw new RuntimeException("error")
			}
			for (port : node.node.proc.out_ports.ports) {
				scope_list.addAll(port.connections)
			}
		} else if (node.node.isIs_behavior) {
			var behavior = EcoreUtil2.getContainerOfType(node, SoflBehaviour)
			for (port : behavior.in_ports.ports) {
				scope_list.addAll(port.connections)
			}
		}
		return classScopeFor(scope_list)
	}

	def getShadowDestinationScope(ShaddowFlowDestinationNode node) {
		var scope_list = new ArrayList<ConnectionName>
		if (node?.node?.proc?.in_ports?.ports !== null) {
			for (port : node.node.proc.in_ports.ports) {
				scope_list.addAll(port.connections)
			}
		} else if (node.node.is_behavior) {
			var behavior = EcoreUtil2.getContainerOfType(node, SoflBehaviour)
			for (port : behavior.out_ports.ports) {
				scope_list.addAll(port.connections)
			}
		}
		return classScopeFor(scope_list)
	}

	def getFlowScope(SoflBehaviour behavior, boolean input) {
		var scope = IScope::NULLSCOPE
		if (input) {
			scope = classScopeFor(behavior.in_ports.ports.map[dec|dec.declare.map[it.identifier]].flatten.flatten,
				scope)
			scope = classScopeFor(behavior.in_ports.ports.map[dec|dec.connections].flatten, scope)
			if (behavior?.adding?.in_added !== null) {
				scope = classScopeFor(behavior.adding.in_added.declare.map[it.identifier].flatten, scope)
				scope = classScopeFor(behavior.adding.in_added.connections, scope)
			}
		} else {
			scope = classScopeFor(behavior.out_ports.ports.map[dec|dec.declare.map[it.identifier]].flatten.flatten,
				scope)
			scope = classScopeFor(behavior.out_ports.ports.map[dec|dec.connections].flatten, scope)
			if (behavior?.adding?.out_added !== null) {
				scope = classScopeFor(behavior.adding.out_added.declare.map[it.identifier].flatten, scope)
				scope = classScopeFor(behavior.adding.out_added.connections, scope)
			}
		}
		scope
	}

	def getFlowScope(SoflProcess process, boolean input) {
		var scope = IScope::NULLSCOPE
		if (process?.in_ports?.ports === null) {
			// throw new RuntimeException("error")
		} else {
			if (input) {
				scope = classScopeFor(process.in_ports.ports.map [ port |
					port.declare.map[it.identifier]
				].flatten.flatten, scope)
				scope = classScopeFor(process.in_ports.ports.map [ port |
					port.connections
				].flatten, scope)
			} else {

				scope = classScopeFor(process.out_ports.ports.map[dec|dec.declare.map[it.identifier]].flatten.flatten,
					scope)
				scope = classScopeFor(process.out_ports.ports.map[dec|dec.connections].flatten, scope)
			}
		}
		scope
	}

	def getFlowScope(BroadcastStructure bcast, boolean input) {
		// /System.out.println("getFlowScope")
		if (bcast === null) {
			throw new RuntimeException("Null")
		}
		var scope = IScope::NULLSCOPE
		// val out = new ArrayList<ConnectVariable>
		if (input) {
			if (bcast.in_variable !== null) {
				scope = classScopeFor(#[bcast.in_variable], scope)
			} else {
				scope = classScopeFor(#[bcast.in_connect], scope)
			}
		} else {
			if (bcast.out_list !== null && bcast.out_list.length > 0) {
				scope = classScopeFor(bcast.out_list, scope)
			} else if (bcast.out_connect !== null) {
				scope = classScopeFor(bcast.out_connect, scope)
			}
		}
		// System.out.println("done - getFlowScope")
		scope
	}

	def getFlow(NodeName object, boolean source) {
		// System.out.println("getFlow")
		if (object === null) {
			throw new RuntimeException("null pointer")
		}
		if (object.is_behavior) {
			var bahavior = EcoreUtil2.getContainerOfType(object, SoflBehaviour)
			getFlowScope(bahavior, source)
		} else if (object.proc !== null) {
			getFlowScope(object.proc, !source)
		} else if (object.broadcast !== null) {
			getFlowScope(object.broadcast, !source)
		} else {
			if (object.cond !== null) {
				if (!source) {
					classScopeFor(#[object.cond.in_variable])
				} else {
					var scope = classScopeFor(object.cond.out_list.filter[it.varname !== null].map[it.varname])
					// classScopeFor(object.cond.out_list.filter[it.connection !== null].map[it.connection],scope)
					scope
				}
			} else if (object.merge !== null) {
				if (!source) {
					classScopeFor(object.merge.invar)
				} else {
					classScopeFor(#[object.merge.out])
				}

			} else if (object.unmerge !== null) {
				if (source) {
					classScopeFor(object.unmerge.out_list)
				} else {
					classScopeFor(#[object.unmerge.invar])
				}
			}
		}
	}

	def IScope getExpressionTermScope(ExpScopeModify exp, IScope scope) {
		// System.out.println("getExpressionTermScope")
		var tmp_scope = if (exp.is_exists || exp.is_forall) {
				var can1 = EcoreUtil2.getAllContentsOfType(exp.bindings, SoflVariableName)
				if (can1.length > 0) {
					classScopeFor(can1, scope)
				} else {
					scope
				}
			} else if (exp.is_let) {
				var can1 = exp.pattern_list.map[it|it.id_list].flatten
				if (can1.length > 0) {
					classScopeFor(can1, scope)
				} else {
					scope
				}
			} else if (exp.set_compress !== null) {
				var lst = EcoreUtil2.getAllContentsOfType(exp.set_compress.new_var, SoflVariableName)
				classScopeFor(lst, scope)
			} else if (exp.var_list !== null) {
				var can1 = exp.var_list.map[it|it.name]
				if (can1.length > 0) {
					classScopeFor(can1, scope)
				} else {
					scope
				}
			} else {
				scope
			}
		var parent = EcoreUtil2.getContainerOfType(exp.eContainer, ExpScopeModify)
		return if (parent === null) {
			// var module = EcoreUtil2.getContainerOfType(exp.eContainer, SoflModule)
			// return classScopeFor(getModuleStoreScope(module).allElements.map[it.EObjectOrProxy],tmp_scope)
			// return tmp_scope
			var precondition = EcoreUtil2.getContainerOfType(exp, PreCondition)
			var n_scope = if (precondition !== null) {
					getPreConditionScope(precondition, tmp_scope)
				} else {
					var postcondition = EcoreUtil2.getContainerOfType(exp, PostCondition)
					if (postcondition !== null) {
						getPostConditionScope(postcondition, tmp_scope)
					} else {
						var condition = EcoreUtil2.getContainerOfType(exp, CondOption)
						if (condition !== null) {
							// var broadcast = EcoreUtil2.getContainerOfType(exp, BroadcastStructure)
							Scopes.scopeFor(#[condition.varname], tmp_scope)
						} else {
							var init = EcoreUtil2.getContainerOfType(exp, SoflInitProcess)
							if (init !== null) {
								getNodeScope(init.eContainer as SoflModule, tmp_scope)
							} else {
								getNodeScope(exp, tmp_scope)

							}
						}
					}
				}
			n_scope
		} else {
			getExpressionTermScope(parent, tmp_scope)
		}
	}

	def getPreConditionScope(PreCondition context, IScope t_scope) {
		var scope = t_scope
		// System.out.println("getPreConditionScope")
		var function = EcoreUtil2.getContainerOfType(context, SoflFunction)
		if (function !== null) {
			scope = classScopeFor(EcoreUtil2.getAllContentsOfType(function.in_ports, SoflVariableName), scope)
		} else {
			var process = EcoreUtil2.getContainerOfType(context, SoflProcess)
			if (process !== null) {
				var state = EcoreUtil2.getContainerOfType(context, Statecondition)
				if (state !== null) {
					if (state.in_port_id - 1 < process.in_ports.ports.length) {
						var port = process.in_ports.ports.get(state.in_port_id - 1)
						scope = classScopeFor(port.declare.map[it.identifier].flatten, scope)
						scope = classScopeFor(port.connections, scope)

					}
				} else {
					scope = classScopeFor(process.in_ports.ports.map[it.declare].flatten, scope)
					scope = classScopeFor(process.in_ports.ports.map[it.connections].flatten, scope)
				}
				if (process.ext !== null) {
					scope = classScopeFor(process.ext.variables.map[it.varariable_name], scope)
				}
			} else {
				var cdfd = EcoreUtil2.getContainerOfType(context, SoflBehaviour)
				if (cdfd !== null) {
					var state = EcoreUtil2.getContainerOfType(context, Statecondition)
					if (state !== null) {
						if (cdfd.adding !== null && cdfd.grouping !== null && cdfd.grouping.in_grouping !== null) {
							if (state.in_port_id - 1 < cdfd.grouping.in_grouping.ports.length) {
								var port = cdfd.grouping.in_grouping.ports.get(state.in_port_id - 1)
								scope = classScopeFor(port.vars, scope)
								scope = classScopeFor(port.connections, scope)
							}
						} else {
							if (state.in_port_id - 1 < cdfd.in_ports.ports.length) {
								var port = cdfd.in_ports.ports.get(state.in_port_id - 1)
								scope = classScopeFor(port.declare.map[it.identifier].flatten, scope)
								scope = classScopeFor(port.connections, scope)
							}
						}
					} else {
						scope = classScopeFor(cdfd.in_ports.ports.map[it.declare].flatten, scope)
						scope = classScopeFor(cdfd.in_ports.ports.map[it.connections].flatten, scope)
					}
					var module = EcoreUtil2.getContainerOfType(cdfd, SoflModule)
					if (module !== null) {
						if (module.const_declare !== null) {
							scope = classScopeFor(module.const_declare.const_list.map[it.name], scope)
						}
						if (module.var_decl !== null) {
							scope = classScopeFor(module.var_decl.vars.map[it.name], scope)
							scope = classScopeFor(module.var_decl.extVars.map [
								if (it.var_name !== null) {
									it.var_name
								} else {
									it.ext_var_name
								}
							], scope)
						}
					}
				} else {
					var module = EcoreUtil2.getContainerOfType(cdfd, SoflModule)
					if (module !== null) {
						if (module.const_declare !== null) {
							scope = classScopeFor(module.const_declare.const_list.map[it.name], scope)
						}
						if (module.var_decl !== null) {
							scope = classScopeFor(module.var_decl.vars.map[it.name], scope)
							scope = classScopeFor(module.var_decl.extVars.map [
								if (it.var_name !== null) {
									it.var_name
								} else {
									it.ext_var_name
								}
							], scope)
						}
					} else {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}
				}
			}
		}
		return scope
	}

	def getPostConditionScope(PostCondition context, IScope t_scope) {
		var scope = t_scope
		var function = EcoreUtil2.getContainerOfType(context, SoflFunction)
		if (function !== null) {
			scope = classScopeFor(#[function.ret_var])
			scope = classScopeFor(EcoreUtil2.getAllContentsOfType(function.in_ports, SoflVariableName), scope)
		} else {
			var process = EcoreUtil2.getContainerOfType(context, SoflProcess)
			if (process !== null) {
				var state = EcoreUtil2.getContainerOfType(context, Statecondition)
				if (state !== null) {
					var iport = process.out_ports.ports.get(state.out_port_id - 1)
					scope = classScopeFor(iport.declare.map[it.identifier].flatten, scope)
					scope = classScopeFor(iport.connections, scope)
					var oport = process.in_ports.ports.get(state.in_port_id - 1)
					scope = classScopeFor(oport.declare.map[it.identifier].flatten, scope)
					scope = classScopeFor(oport.connections, scope)
				} else {
					scope = classScopeFor(process.out_ports.ports.map[it.declare].flatten, scope)
					scope = classScopeFor(process.out_ports.ports.map[it.connections].flatten, scope)
					scope = classScopeFor(process.in_ports.ports.map[it.declare].flatten, scope)
					scope = classScopeFor(process.in_ports.ports.map[it.connections].flatten, scope)
				}
				if (process.ext !== null) {
					scope = classScopeFor(process.ext.variables.map[it.varariable_name], scope)
				}
			} else {
				var cdfd = EcoreUtil2.getContainerOfType(context, SoflBehaviour)
				if (cdfd !== null) {
					var state = EcoreUtil2.getContainerOfType(context, Statecondition)
					if (state !== null) {
						if (cdfd.grouping !== null) {
							if (state.out_port_id - 1 < cdfd.grouping.out_grouping.ports.length) {
								var port = cdfd.grouping.out_grouping.ports.get(state.out_port_id - 1)
								scope = classScopeFor(port.vars, scope)
								scope = classScopeFor(port.connections, scope)
							}
						} else {
							if (state.out_port_id - 1 < cdfd.out_ports.ports.length) {
								var port = cdfd.out_ports.ports.get(state.out_port_id - 1)
								scope = classScopeFor(port.declare.map[it.identifier].flatten, scope)
								scope = classScopeFor(port.connections, scope)
							}
						}
					} else {
						scope = classScopeFor(cdfd.out_ports.ports.map[it.declare].flatten, scope)
						scope = classScopeFor(cdfd.out_ports.ports.map[it.connections].flatten, scope)
					}
					var module = EcoreUtil2.getContainerOfType(cdfd, SoflModule)
					if (module !== null) {
						if (module.const_declare !== null) {
							scope = classScopeFor(module.const_declare.const_list.map[it.name], scope)
						}
						if (module.var_decl !== null) {
							scope = classScopeFor(module.var_decl.vars.map[it.name], scope)
							scope = classScopeFor(module.var_decl.extVars.map [
								if (it.var_name !== null) {
									it.var_name
								} else {
									it.ext_var_name
								}
							], scope)
							scope = scope
						}
					}
				} else {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}
			}
		}
		return scope
	}

	def getNodeScope(ExpScopeModify context, IScope t_scope) {
		var scope = t_scope
		// System.out.println("getNodeScope")
		var function = EcoreUtil2.getContainerOfType(context, SoflFunction)
		var scope_list = if (function !== null) {
				var tmp = EcoreUtil2.getAllContentsOfType(function.in_ports, SoflVariableName)
				tmp.add(function.ret_var)
				tmp
			} else {
				var condition = EcoreUtil2.getContainerOfType(context, CondOption)
				if (condition !== null) {
					#[condition.varname]
				} else {
					var module = EcoreUtil2.getContainerOfType(context, SoflModule)
					if (module !== null) {
						var lst = EcoreUtil2.getAllContentsOfType(module.var_decl, SoflVariableName)
						lst
					} else {
						#[]

					}
				}
			}
		scope = classScopeFor(scope_list, scope)
		var module = EcoreUtil2.getContainerOfType(context, SoflModule)
		if (module !== null) {
			scope = getNodeScope(module, scope)
		}
	}

	def getNodeScope(SoflModule module, IScope t_scope) {
		var scope = t_scope
		if (module.const_declare !== null) {
			scope = classScopeFor(module.const_declare.const_list.map[it.name], scope)
		}
		if (module.var_decl !== null) {
			scope = classScopeFor(module.var_decl.vars.map[it.name], scope)
			scope = classScopeFor(module.var_decl.extVars.map [
				if (it.var_name !== null) {
					it.var_name
				} else {
					it.ext_var_name
				}
			], scope)
		}
		return scope
	}
}
