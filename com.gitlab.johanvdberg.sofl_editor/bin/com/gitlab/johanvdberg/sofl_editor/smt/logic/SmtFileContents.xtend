package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.Map
import com.gitlab.johanvdberg.sofl_editor.types.Natural0Type
import com.gitlab.johanvdberg.sofl_editor.types.NaturalType
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.types.BooleanType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition

class SmtFileContents {
	public String contents
	
	
	new(){
		this.contents = 
		'''
		(set-option :print-success false)
		(set-logic ALL)
		'''
	}
	def add_variable(String variable_name, TypeBase type){
		this.contents =
		'''
		«this.contents»
		(declare-const «variable_name» «SmtCondition::smt_type(type)»)
		'''
		if(type instanceof BooleanType){
			this.add_assert_formula('''(or (= «variable_name» 0) (= «variable_name» 1) )''')
		}else if(type instanceof Natural0Type){
			this.add_assert_formula('''(>= «variable_name» 0)''')
		}else if(type instanceof NaturalType){
			this.add_assert_formula('''(> «variable_name» 0)''')
		}
	}
	
	def add_print(String str){
		this.contents = 
		'''
		«this.contents»
		(echo "«str»")
		'''
	}
	
	def add_assert_formula(String formula){
		this.contents = 
		'''
		«this.contents»
		(assert «formula»)
		'''
	}
	
	def add_assert_formula(Map<String, TypeBase> variables, String formula){
		this.contents = 
		'''
		«this.contents»
		(assert (forall («FOR v: variables.keySet» («v» («variables.get(v)»)) «ENDFOR») «formula»))
		'''
	}
	
	def add_pop(){
			this.contents =
		'''
		«this.contents»
		(pop)		
		'''
	}
	
	def add_push(){
			this.contents =
		'''
		«this.contents»
		(push)		
		'''
	}
	def add_sat_check(){
		this.contents =
		'''
		«this.contents»
		(check-sat)
		'''
	}
}