package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.ArrayList
import java.util.List
import java.util.Map
import java.util.TreeSet
import com.gitlab.johanvdberg.sofl_editor.smt.SmtPort
import com.gitlab.johanvdberg.sofl_editor.smt.SmtPortInNode

class ProofObligationFlowPredicate extends ProofObligationImply {
	public List<SmtCondition> environment
	public List<SmtCondition> invariants	
	public SmtCondition obligation
	public Map<String, TreeSet<SmtPort>> pre_image
	
	def static create(SmtPortInNode iport, int out_index){
		val out = new ArrayList<ProofObligationFlowPredicate>
		out.add(new ProofObligationFlowPredicate(iport,  out_index))				
		return out
	}
	
	protected new (SmtPortInNode port, int out_index){
		val port_condition = port.get_condition(out_index) //.get_condition(out_index)
		this.obligation_name = '''Pre image in«port.index+1»  out «out_index + 1»'''
		this.environment = new ArrayList<SmtCondition>
		this.invariants = new ArrayList<SmtCondition>
				
		for(c_port: port.get_connected_ports){
			this.environment.add(c_port.get_codition)
		}
		this.obligation = port_condition
		this.invariants.addAll(port.parentNode.parent.invariants)
		
		super.assign_condition((invariants.toList + emptyList).toList, (invariants.toList + #[obligation]).toList)
	}
}
