package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.Map
import java.util.List
import java.util.HashMap
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAnd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpOr
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpImply
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNeq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAdd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMinus
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMult
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpIntDiv
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrt
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrtEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLessEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpScopeModify
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNumber
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpBool
import com.gitlab.johanvdberg.sofl_editor.types.IntegerType
import com.gitlab.johanvdberg.sofl_editor.types.Natural0Type
import com.gitlab.johanvdberg.sofl_editor.types.NaturalType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.VariableDeclare
import com.gitlab.johanvdberg.sofl_editor.types.BooleanType
import com.gitlab.johanvdberg.sofl_editor.evaluate.Evaluate

class SmtFormula {
	
	public String formula
	public Map<String, TypeBase> variables;
	
	new(Expression exp){
		this.variables = new HashMap<String, TypeBase>
		this.formula = this.smt_formula(exp)
	}
	
	def String smt_formula(Expression exp){
		if(exp instanceof ExpAnd){
			'''(and «smt_formula(exp.left)» «smt_formula(exp.right)»)'''		
		}else if(exp instanceof ExpOr){
			'''(or «smt_formula(exp.left)» «smt_formula(exp.right)»)'''		
		}else if(exp instanceof ExpImply){
			'''(=> «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if(exp instanceof ExpEq){
			'''(= «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if(exp instanceof ExpNeq){
			'''(not(= «smt_formula(exp.left)» «smt_formula(exp.right)»))'''	
		}else if(exp instanceof ExpAdd){
			'''(+ «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if(exp instanceof ExpMinus){
			'''(- «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if(exp instanceof ExpMult){
			'''(* «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if(exp instanceof ExpIntDiv){
			'''(/ «smt_formula(exp.left)» «smt_formula(exp.right)»)'''	
		}else if (exp instanceof ExpGrt){
			'''(> «smt_formula(exp.left)» «smt_formula(exp.right)»)'''
		}else if (exp instanceof ExpLess){
			'''(< «smt_formula(exp.left)» «smt_formula(exp.right)»)'''
		}else if (exp instanceof ExpGrtEq){
			'''(>= «smt_formula(exp.left)» «smt_formula(exp.right)»)'''
		}else if (exp instanceof ExpLessEq){
			'''(<= «smt_formula(exp.left)» «smt_formula(exp.right)»)'''
		}else if(exp instanceof ExpScopeModify){
			if(exp.variable !== null){
				var variable = if(exp.is_un_mod){
					't_' + exp.variable.name
				}else{
					exp.variable.name
				}
				if(this.variables.containsKey(variable)){
					//throw new RuntimeException("Duplicate variable name")
				}else{
					var type = ExpressionType.getType(exp)
					this.variables.put(variable, type)
				}
				return variable
			}else if(exp.is_grouping){
				'''(«smt_formula(exp.expression)»)'''
			}else if(exp.is_forall){
				//get the variables created
				var newVar = this.getVaraiableSet(exp.bindings.declare.toList)
				//keep a copy of existing duplicate names
				var store = new HashMap<String,TypeBase>
				for(tk: newVar.keySet){
					if(this.variables.containsKey(tk)){
						store.put(tk, this.variables.get(tk))
						this.variables.remove(tk)
					}
				}
				//create formula end parse exp.expression
				var formula = '''(forall («FOR v: newVar.keySet» («v» «smt_type(newVar.get(v))») «ENDFOR») «this.smt_formula(exp.expression)»)'''
				//remove create variable from the free variable set
				for(tk: newVar.keySet){
					if(this.variables.containsKey(tk)){
						this.variables.remove(tk)
					}
				}
				//restore the stored variables.
				for(tk: store.keySet){
					this.variables.put(tk, this.variables.get(tk))
				}							
				return formula				
			}else{
				'''undefined 1 «exp»'''
			}			
		}else if(exp instanceof ExpNumber){
			Evaluate.evaluate(exp.number).toString
		}else if(exp instanceof ExpBool){
			if(exp.is_true){
				'true'
			}else{
				'false'
			}
		}else{
			'''undefined 2 «exp»'''
		}
	}
	
	
	def smt_type(TypeBase type){
		if(type instanceof IntegerType){
			"Int"
		}else if(type instanceof Natural0Type){
			"Int"
		}else if(type instanceof NaturalType){
			"Int"
		}else if(type instanceof BooleanType){
			"Bool"
		}else{
			"smt_type"
		}
	}
	def getVaraiableSet(List<VariableDeclare> lst){
		val map = new HashMap<String, TypeBase>
		for(v: lst){
			val t = ExpressionType.getType(v.type)
			v.identifier.forEach[map.put(it.name, t)]
		}
		return map
	}
}
