package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.SmtPort
import java.util.ArrayList
import java.util.List

class ProofObligationRefinementUp extends ProofObligationImply {
	public List<SmtCondition> upper_obligation
	public List<SmtCondition> upper_invariants
	public List<SmtCondition> lower_invariants	
	public SmtCondition lower_obligation
	
	new(List<SmtCondition> lower_condition, SmtPort source_port, SmtPort refined_port, List<SmtCondition> upper_conditions) {
		this.upper_invariants = new ArrayList<SmtCondition>
		this.lower_invariants = new ArrayList<SmtCondition>
		this.upper_obligation = new ArrayList<SmtCondition>
		
		this.upper_obligation.add(refined_port.condition)
		this.lower_invariants.addAll(lower_condition)
		this.upper_invariants.addAll(upper_conditions)
		this.lower_obligation = source_port.condition
		
		super.assign_condition((this.lower_invariants.toList + #[this.lower_obligation]).toList,
			(this.upper_invariants.toList + this.upper_obligation).toList
		)
	}
	
	
}
