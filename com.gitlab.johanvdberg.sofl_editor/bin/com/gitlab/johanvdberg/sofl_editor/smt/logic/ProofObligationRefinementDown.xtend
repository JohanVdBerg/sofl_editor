package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.ArrayList
import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.SmtPort

//start invariant - by super module
//environment by port of process begin refined - for all over these variables and invariant
// the target port and the invariant of the lower module given the condition to be valid.
class ProofObligationRefinementDown extends ProofObligationImply {
	public List<SmtCondition> refined_port_conditions
	public List<SmtCondition> upper_invariants
	public List<SmtCondition> lower_invariants	
	public List<SmtCondition> init_condition
	public SmtCondition obligation

	
	new(List<SmtCondition> upper_invariant, SmtPort refined_list, SmtPort target_port, Iterable<SmtCondition> lower_invariant){
		//super(target_port)
		this.upper_invariants = new ArrayList<SmtCondition>
		this.lower_invariants = new ArrayList<SmtCondition>
		this.init_condition = new ArrayList<SmtCondition>
		this.refined_port_conditions = #[refined_list.condition]
		
		this.obligation = target_port.condition
		this.lower_invariants.addAll(lower_invariant)
		this.upper_invariants.addAll(upper_invariant)
			
		super.assign_condition((this.upper_invariants.toList + this.init_condition).toList, (this.lower_invariants.toList + this.refined_port_conditions).toList)
	}
	
	
}
