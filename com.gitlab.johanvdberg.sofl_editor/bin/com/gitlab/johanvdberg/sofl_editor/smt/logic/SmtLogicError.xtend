package com.gitlab.johanvdberg.sofl_editor.smt.logic

class SmtLogicError extends Logic{
	
	public String err
	
	new(String err){
		super(Operation.ERROR)
		this.err = err
	}
}
