package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligation
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationPredicateTransform
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementDown
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementUp
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationSatisfiable
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationFlowPredicate

class SmtBehviorNode extends SmtNode {
	public Map<String, ProofObligation> refinement_up
	public Map<String, ProofObligation> refinement_down
	//public Map<String, ProofObligation> out_flow

	public List<SmtConditionNode> condition_nodes

	protected new(SoflBehaviour node, SmtModule parent) {
		super(node.state)
		this.refinement_up = new HashMap<String, ProofObligation>
		this.refinement_down = new HashMap<String, ProofObligation>
		this.condition_nodes = new ArrayList<SmtConditionNode>
		this.parent = parent
		this.node_name = node.name
		this.in_ports = new ArrayList<SmtPortInNode>
		//this.out_ports = new ArrayList<SmtPortInNode>
		val oports = if (node?.grouping?.out_grouping?.ports !== null) {
				SmtPort.create(this, node.out_ports, node.adding.out_added, node.grouping.out_grouping,
					SmtCondition.getConditionlist(node.state, false), true)
			} else if (node.state !== null) {
				SmtPort.create(this, node.out_ports, SmtCondition.getConditionlist(node.state, false), true)
			} else {
				val l = VariableType::create(node.out_ports)
				var out = new ArrayList<SmtPort>
				for (port_index : 0 ..< l.length) {
					// get the conditions from the condition of the CDFD
					var p = new SmtPortInNode(this, port_index, l.get(port_index), SmtCondition::create(true),
						node.in_ports)
					p.set_as_destination(true)
					out.add(p)
				}
				out
			}
		val iports = if (node?.grouping?.in_grouping?.ports !== null) {
				SmtPort.create(this, node.in_ports, node.adding.in_added, node.grouping.in_grouping,
					SmtCondition.getConditionlist(node.state, true), false)
			} else if (node.state !== null) {
				SmtPort.create(this, node.in_ports, SmtCondition.getConditionlist(node.state, true), false)
			} else {
				val l = VariableType::create(node.in_ports)
				var out = new ArrayList<SmtPort>
				for (port_index : 0 ..< l.length) {
					// get the conditions from the condition of the CDFD
					var p = new SmtPortInNode(this, port_index, l.get(port_index), SmtCondition::create(true),
						node.out_ports)
					p.set_as_destination(false)
					out.add(p)
				}
				out
			}

		for (port : oports) {
			this.out_ports.add(port as SmtPortInNode)
		}
		for (port : iports) {
			this.in_ports.add(port as SmtPortInNode)
		}

		// add condition nodes
		if (node.conditions !== null) {
			for (c_node : node.conditions) {
				this.condition_nodes.add(new SmtConditionNode(c_node, this.parent))
			}
		}
	}

	override computeProofObligations() {
		// if refinement relation exist
		if (this.parent.have_parent) {
			var refined_process = this.parent.get_process_being_refined()
			// var o_r_port_list = refined_process.out_ports
			var upper_invariant = new ArrayList<SmtCondition>
			var parent_parent = this.parent.get_parent

			upper_invariant.addAll(parent_parent.invariants)

			for (oport : this.out_ports) {
				var refined_ports = refined_process.out_ports.filter[SmtPort::constain_varaibles(it, oport)]
				if (refined_ports.length != 1) {
					throw new RuntimeException("error")
				}
				this.refinement_up.put(
					'''«oport.index»''',
					new ProofObligationRefinementUp(this.parent.invariants, oport, refined_ports.head, upper_invariant)
				)
			}

			// upper_invariant.addAll(parent_parent.initInvariants)
			// / XXX what to do with the invariant
			for (iport : this.in_ports) {
				var refined_ports = refined_process.in_ports.filter[SmtPort::constain_varaibles(it, iport)]
				if (refined_ports.length != 1) {
					throw new RuntimeException("error")
				}
				this.refinement_down.put(
					'''«iport.index»''',
					new ProofObligationRefinementDown(upper_invariant, refined_ports.head, iport,
						this.parent.invariants)
				)
			}

		}
		//compute obligations of the output ports	
		for (start_port : this.in_ports) {	
			for (end_port : this.out_ports) {
				var obligation = ProofObligationPredicateTransform::create(get_port_invariant(start_port), start_port, end_port, get_port_invariant(end_port))			
				this.transition_obligations.put(new SmtTransitionPorts(start_port.index, end_port.index), obligation)
			}
		}
		for (port : this.get_destination_ports()) {
			// write the test for the environment
			// var image =new SmtPreImage(port)
			for (out_port : this.get_source_ports()) {
				var obligation = ProofObligationFlowPredicate::create(port, out_port.index)
				obligation.forEach[this.flow_port_obligations.put(it.obligation_name, it)]
			}
		}

	}

	override have_parent() {
		this.parent.have_parent
	}

	override get_source_ports() {
		this.in_ports
	}

	override get_destination_ports() {
		this.out_ports
	}

	def get_refinement_obligation() {
		if (this.parent.have_parent) {
			var relation = new HashMap<String, ProofObligation>

			for (ik : this.refinement_down.keySet) {
				relation.put('''Refine Down «ik»''', this.refinement_down.get(ik))
			}

			for (ok : this.refinement_up.keySet) {
				relation.put('''Refine Up «ok»''', this.refinement_up.get(ok))
			}

			relation
		} else {
			throw new RuntimeException("Must have parent to be allowed to call this method")
		}
	}

	override SmtCondition getDestinationPredicate(int input, int output) {
		super.getSourcePredicate(output, input)
	}

	override SmtCondition getSourcePredicate(int input, int output) {
		super.getDestinationPredicate(output, input)
	}

}
