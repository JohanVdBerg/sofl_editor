package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.semantics.TypeFactory
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.DataFlowReference
import com.gitlab.johanvdberg.sofl_editor.soflDsl.DataFlows
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PortDeclarations
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.StateConditionsDefinition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.VariableDeclare
import com.gitlab.johanvdberg.sofl_editor.types.ConnectionType
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Set
import org.eclipse.emf.ecore.EObject

//the ports of some node does not have a condition but inherit the condition from the port
//from a predecessor of the input port of the parent node.
//to set the condition of such a node the data flow diagram must be walked through to obtain the condition.
class SmtPort implements Comparable<SmtPort> {
	protected int index
	protected Set<VariableType> variable_list
	public SmtCondition condition
	public EObject syntax_port

	protected new() {
		this.variable_list = new HashSet<VariableType>
	}

	protected new(List<VariableDeclare> declares, SmtCondition condition) {
		this.syntax_port = declares.head
		this.variable_list = new HashSet<VariableType>
		for (dec : declares) {
			val type = TypeFactory::getType(dec.type)
			this.variable_list.addAll(
				dec.identifier.map[new VariableType(it.name, type)]
			)
		}
		this.condition = condition
	}

	new(PortDeclarations flows, SmtCondition condition) {
		this.syntax_port = flows
		this.variable_list = new HashSet<VariableType>
		this.condition = condition
		this.variable_list.addAll(VariableType::create(flows))
	}

	new(VariableType variable) {
		this.variable_list = new HashSet<VariableType>
		this.variable_list.add(variable)
		this.syntax_port = null
		this.condition = SmtCondition::create(true)
	}


	def get_variables(){
		this.variable_list
	}
	
	def get_codition() {
		this.condition
	}

	def contains_variable(String string) {
		return this.variable_list.exists[it|it.varaible == string]
	}

	def getIndex() {
		this.index
	}

	def get_name() {
		return '''port:«this.index»'''
	}

	override compareTo(SmtPort o) {
		this.get_name.compareTo(o.get_name)
	}

	def get_condition() {
		this.condition
	}

	def get_variable_count() {
		this.variable_list.length
	}

	def static variablePortRefinesPort(SmtPort refinedPort, SmtPort refPort) {
		if (refinedPort.get_variable_count < refPort.get_variable_count) {
			false
		} else {
			refinedPort.variable_list.contains(refinedPort.variable_list)
		}
	}

	def static create(List<VariableDeclare> decl_list, SmtCondition condition) {
		new SmtPort(decl_list, condition)
	}

	def static create(VariableType variable) {
		new SmtPort(variable)
	}

	def static create(PortDeclarations port, SmtCondition condition) {
		new SmtPort(port, condition)
	}

	protected def static create_port(DataFlows flows, StateConditionsDefinition conditions, boolean destination) {
		var out = new ArrayList<SmtPort>
		var index = 1
		for (port : flows.ports) {
			val idx = index
			var condition_list = conditions.state_list.filter [
				if (destination) {
					it.in_port_id == idx
				} else {
					it.out_port_id == idx
				}
			].map [
				if (destination) {
					SmtCondition::create(it.pre.condition)
				} else {
					SmtCondition::create(it.post.condition)
				}
			]
			out.add(SmtPort::create(port, SmtCondition::disjunction(condition_list.toList)))

			index += 1
		}
		out
	}

	protected def static create(
		SmtNode parent,
		DataFlows base_decl,
		PortDeclarations new_var,
		DataFlowReference grouping,
		List<SmtCondition> conditionList,
		boolean destination
	) {
		createInNode(parent, base_decl, new_var, grouping, conditionList, destination)
	}

	protected def static createInNode(
		SmtNode parent,
		DataFlows base_decl,
		PortDeclarations new_var,
		DataFlowReference grouping,
		List<SmtCondition> conditionList,
		boolean destination
	) {
		// create a map of all variables and 
		val var_map = new HashMap<String, TypeBase>
		for (port : base_decl.ports) {
			VariableType.create(port).forEach [ it |
				var_map.put(it.varaible, it.type)
			]
		}
		if (new_var !== null) {
			VariableType.create(new_var).forEach [ it |
				var_map.put(it.varaible, it.type)
			]
		}
		// their type names
		var port_list = new ArrayList<SmtPort>
		var port_index = 0
		if (grouping.ports !== null) {
			for (port : grouping.ports) {
				val v_list = new ArrayList<VariableType>
				port.connections.forEach[v_list.add(new VariableType(it.name, new ConnectionType))]
				// v_list.add(new VariableType(port.connection.name, var_map.get(port.connection.name)))				
				port.vars.forall[v_list.add(new VariableType(it.name, var_map.get(it.name)))]
				// create a port list from their grouping		
				for (index : 0 ..< port_index + 1) {
					var tmp_port = new SmtPortInNode(parent, port_index, v_list, conditionList.get(index), grouping)
					tmp_port.set_as_destination(destination)
					port_list.add(tmp_port)
				}
				port_index = port_index + 1
			}
		}
		port_list
	}

	protected def static create(
		SmtNode parent,
		DataFlows base_decl,
		List<SmtCondition> conditionList,
		boolean destination
	) {
		// create a map of all variables and 
		val var_map = new HashMap<String, TypeBase>
		for (port : base_decl.ports) {
			VariableType.create(port).forEach [ it |
				var_map.put(it.varaible, it.type)
			]
		}

		// their type names
		var port_list = new ArrayList<SmtPort>
		var port_index = 0
		for (port : base_decl.ports) {
			val v_list = new ArrayList<VariableType>
			if (port.connections.length > 0) {
				port.connections.forEach [
					v_list.add(new VariableType(it.name, new ConnectionType))
				]
			// v_list.add(new VariableType(port.connection.name, var_map.get(port.connection.name)))
			}
			if (port.declare.length > 0) {
				port.declare.forall [ it |
					it.identifier.forall[v|v_list.add(new VariableType(v.name, var_map.get(v.name)))]
				]
			}
			// create a port list from their grouping		
			// for(index:0..<port_index + 1){
			var tmp_port = new SmtPortInNode(parent, port_index, v_list, conditionList.get(port_index), base_decl)
			tmp_port.set_as_destination(destination)
			port_list.add(tmp_port)
			// }
			port_index = port_index + 1

		}
		port_list
	}

	def static getType(SoflType type) {
		ExpressionType.getType(type)
	}

	def get_index() {
		this.index
	}

	def static constain_varaibles(SmtPort subset, SmtPort superser) {
		for (v : subset.variable_list) {
			if (! superser.variable_list.contains(v)) {
				return false
			}
		}
		return true;
	}

}
