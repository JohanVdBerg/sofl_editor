package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.semantics.Node.NodeType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationCondition
import com.gitlab.johanvdberg.sofl_editor.types.VariableType

class SmtConditionNode extends SmtStructureNode{
	protected new(ConditionStructure node, SmtModule parent){
		super(NodeType::Condition)		
		this.parent = parent		
		this.node_name = node.name
		var cond_list = node.out_list.map[SmtCondition::create(it.pred)]
		this.add_port(#[new VariableType(node.in_variable.name)], SmtCondition::disjunction(cond_list), true, node)
		for(out: node.out_list){
			this.add_port(#[new VariableType(out.varname.name)], SmtCondition::create(out.pred), false, node)
		}
		this.transition_obligations.put(new SmtTransitionPorts(1,-1), new ProofObligationCondition(cond_list))
	}
	
	override computeProofObligations() {
		
	}
}