package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.List
import java.util.TreeSet
import java.util.ArrayList
import org.eclipse.emf.ecore.EObject
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.types.VariableType

class SmtPortInNode extends SmtPort{
	//protected List<SmtPortInNode> destination_port
	protected List<SmtPortInNode> connected_ports
	protected SmtNode node_parent
	protected boolean destination
	
	new(){
		this.variable_list = new TreeSet<VariableType>
		//this.destination_port = new ArrayList<SmtPortInNode>
		this.connected_ports = new ArrayList<SmtPortInNode>
	}
	new(SmtNode parent, int index, List<VariableType> varaible_list, SmtCondition condition, EObject syntax_port){
		this.variable_list = new TreeSet<VariableType>
		//this.destination_port = new ArrayList<SmtPortInNode>
		this.connected_ports = new ArrayList<SmtPortInNode>
		this.node_parent = parent
		this.index = index
		this.variable_list.addAll(varaible_list)
		//this.input = input	
		this.condition = condition
		this.syntax_port = syntax_port
	}
	
	
	def get_condition(int other){
		if(this.destination){
			this.node_parent.getDestinationPredicate(this.index, other)	
		}else{
			this.node_parent.getSourcePredicate(this.index, other)
		}		
	}
	
	def get_connected_ports(){
		return this.connected_ports
	}
	
	/* 
	def get_destination_list(){
		return this.destination_port
	}*/
	
	def set_as_destination(boolean destination){
		this.destination = destination
	}	
	
	def String getParentName(){
		this.node_parent.name
	}
	
	def getParentNode(){
		this.node_parent
	}
	
	override get_name(){
		var io = if(this.destination){
			'in'
		}else{
			'out'
		}
		return '''«this.node_parent.name»_«io»_«this.index»'''
	}
			
	def addConectedPort(SmtPortInNode port){
		this.connected_ports.add(port)
	}
	
}