/*
 * generated by Xtext 2.14.0
 */
package com.gitlab.johanvdberg.sofl_editor


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class SoflDslStandaloneSetup extends SoflDslStandaloneSetupGenerated {

	def static void doSetup() {
		new SoflDslStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
